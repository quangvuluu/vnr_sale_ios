//
//  InfoViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/1/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController, NavBarDelegate {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var userTitle: UILabel!
    @IBOutlet weak var userAddress: UILabel!
    @IBOutlet weak var oldPass: UITextField!
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var retype: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btNext: UIButton!
    
    @IBOutlet weak var scrollConstraint: NSLayoutConstraint!
    var keyboardHandler : KeyboardHandler!
    
    var navBarTitle: String {
        return "TÀI KHOẢN"
    }
    
    var navBarTitleColor: UIColor {
        return UIColor.GGGreen
    }
    
    var navBarBgColor: UIColor {
        return UIColor.white
    }
    
    func navBarBtnIcon(menu: Bool) -> UIImage? {
        return UIImage(named: "icon-menu-green.png")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUserView(Glob.user)
        keyboardHandler = KeyboardHandler(view: self.view, bottomConstr: self.scrollConstraint)
        self.view.dismissKeyboardOnTap()
    }

    func setUserView(_ user : User) {
        userName.text = user.fullName
        userPhone.text = user.phone
        userEmail.isHidden = true
        userAddress.isHidden = true
        
        let isASM = Glob.user.roles.contains("ASM")
        userTitle.text = isASM ? "Trưởng nhóm bán hàng" : "Nhân viên bán hàng"
    }
    
    @IBAction func changePasswordClick(_ sender: UIButton) {
        if Utils.isEmpty(oldPass.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập mật khẩu cũ.")
            return
        }
        else if Utils.isEmpty(newPass.text){
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập mật khẩu mới.")
            return
        }
        else if !(newPass.text == retype.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Mật khẩu mới và mật khẩu nhập lại không trùng khớp.")
            return
        }
        else {
            GHAPI.api.changePass(oldPassword: oldPass.text ?? "", newPassword: newPass.text ?? "").done {res in
                Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Thay đổi mật khẩu thành công.")
                self.clearTextField()
                }.catch { err in
                    Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Thay đổi mật khẩu không thành công.")
                }
        }
    }
    
    func clearTextField() {
        oldPass.text = ""
        newPass.text = ""
        retype.text = ""
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardHandler.attachListener()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        keyboardHandler.removeListener()
    }
}
