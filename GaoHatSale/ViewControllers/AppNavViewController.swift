//
//  AppNavViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/2/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import SideMenuSwift

class AppNavViewController: SideMenuController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        let mainViewController = MainViewController(nibName: "MainViewController", bundle: nil)
        self.contentViewController = mainViewController
        self.menuViewController = MenuViewController(nibName: "MenuViewController", bundle: nil)
        
        mainViewController.menuNavController = self
        (self.menuViewController as? MenuViewController)?.delegate = mainViewController
        
        SideMenuController.preferences.basic.menuWidth = 0.8 * UIScreen.main.bounds.width
        SideMenuController.preferences.basic.direction = .left
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
