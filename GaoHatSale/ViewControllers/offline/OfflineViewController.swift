//
//  OfflineViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/18/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OfflineViewController: UIViewController {
    
    @IBOutlet weak var btNetwork: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    private func showMainController() -> Void {
        DispatchQueue.main.async {
//                self.performSegue(withIdentifier: "LoginViewController", sender: self)
            let controller = LoadingViewController()
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func onReloadClick(_ sender: Any) {
//        self.startNVIndicator(data: ActivityData(minimumDisplayTime: 2000))
        print("Start checking reachability")
        if self.isReachable {
            print("Reachable")
            self.stopNVIndicator()
            self.showMainController()
        }
        else {
            print("Unreachable")
            DispatchQueue.main.async {
                self.stopNVIndicator()
            }
        }
    }
}
