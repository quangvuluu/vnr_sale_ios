//
//  MenuViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/2/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate {
    func onMenuPressed(item: String)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var delegate: MenuViewControllerDelegate?
    @IBOutlet weak var tbMenu: UITableView!
    
    var selectedTab = 0
    
    var menuItems : Array<String> = ["BÁN HÀNG", "TÀI KHOẢN", "TÍN DỤNG", "LỊCH SỬ","GIỚI THIỆU", "ĐĂNG XUẤT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        self.setupTable()
    }
    
    // Data source
    func setupTable() {
        tbMenu.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "menuCell")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as!  MenuTableViewCell
        
        let item = menuItems[indexPath.row]
        cell.setupCell(title: item, isSelected: (selectedTab == indexPath.row))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        self.delegate?.onMenuPressed(item: menuItems[indexPath.row])
        selectedTab = indexPath.row
        
        tableView.reloadData()
    }
}
