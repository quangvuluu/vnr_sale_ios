//
//  MenuTableViewCell.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/5/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(title: String, isSelected: Bool) {
        lblTitle.text = title
        if isSelected {
            lblTitle.textColor = Utils.UIColorFromRGB(red: 19, green: 149, blue: 77)
        } else {
            lblTitle.textColor = UIColor.black
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
