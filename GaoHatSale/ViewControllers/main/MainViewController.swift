//
//  MainViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/2/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import SideMenuSwift
import PromiseKit

import UIKit
import SideMenuSwift
import PromiseKit

@objc protocol NavBarDelegate {
    @objc optional var navBarTitle: String { get }
    @objc optional var navBarTitleColor: UIColor { get }
    @objc optional func navBarBtnIcon(menu: Bool) -> UIImage?
    @objc optional var navBarBgColor: UIColor { get }
}

class MainViewController: UIViewController, MenuViewControllerDelegate, UINavigationControllerDelegate {
    public var menuNavController: SideMenuController?
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var mainView: ContainerView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var headerView: UIView!
    
    var navVC: UINavigationController!
    var lastItem: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navVC = UINavigationController(nibName: nil, bundle: nil)
        self.navVC.isNavigationBarHidden = true
        self.navVC.delegate = self
        let _ = self.mainView.embedViewController(self.navVC)
        
        self.startNVIndicator()
        GHAPI.api.getProducts().then { products -> Promise<User> in
            Glob.allProducts = products
            return GHAPI.api.getProfile()
        }.then({ [weak self] user -> Promise<Void> in
            Glob.user = user
            self?.onMenuPressed(item: "BÁN HÀNG")
            return Promise()
        }).done({
            self.stopNVIndicator()
        })
    }
    
       @IBAction func btnMenuPressed(_ sender: Any) {
           if (self.navVC.viewControllers.count > 1) {
               self.navVC.popViewController(animated: true)
           }
           else {
               self.menuNavController?.revealMenu()
           }
           
           self.view._doDismissKeyboard()
       }
 
     func onMenuPressed(item: String) {
         var vc: UIViewController?
         switch item {
         case "BÁN HÀNG":
             vc = SaleViewController(nibName: "SaleViewController", bundle: nil)
             break
             
         case "TÀI KHOẢN":
             vc = AccountViewController(nibName: "AccountViewController", bundle: nil)
             break
             
         case "TÍN DỤNG":
             vc = BalanceViewController(nibName: "BalanceViewController", bundle: nil)
             break
         
         case "LỊCH SỬ":
             vc = OrdersViewController(nibName: "OrdersViewController", bundle: nil)
             break
             
         case "GIỚI THIỆU":
             vc = RefViewController(nibName: "RefViewController", bundle: nil)
             break
             
         case "ĐĂNG XUẤT":
             Utils.showYesNoAlertPanel(self, title: "", msg: "Bạn có chắc chắn muốn đăng xuất.", yes: "Có", no: "Không") { (isYes) in
                 if (!isYes) {
                     return
                 }
                 
                 GHAPI.api.logout()
                 
                 Glob.reloadAllCaches()
                 UserDefaults.standard.set("", forKey: "access_token")
                 
                 let window = UIApplication.shared.delegate?.window!
                 vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                 window?.rootViewController = vc
                 window?.makeKeyAndVisible()
             }
             
             break
             
         default: break
             
         }
         
         menuNavController?.hideMenu()
         if item != "ĐĂNG XUẤT" {
             lastItem = item
         }
         
         if let vc = vc {
             self.navVC.viewControllers = [vc]
         }
     }
     
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navVC?.navigationBar.barStyle = .black
    }

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        self.customizeNavBar(nav: navigationController, vc: viewController)
    }
    
    func customizeNavBar(nav: UINavigationController, vc: UIViewController) {
        let navDelegate = vc as? NavBarDelegate
        self.lbTitle.text = navDelegate?.navBarTitle ?? self.lastItem ?? ""
        if (ENV == .Staging) {
            self.lbTitle.text = "\(self.lbTitle.text!) (STAGING)"
        }
        self.lbTitle.textColor = navDelegate?.navBarTitleColor ?? UIColor.white
        
        let isMenu = nav.viewControllers.count <= 1
        let btnImg = navDelegate?.navBarBtnIcon?(menu: isMenu) ?? UIImage(named: isMenu ? "icon-menu-white.png" : "icon-back-white.png")
        self.btnMenu.setBackgroundImage(btnImg, for: .normal)
        
        let bgColor = navDelegate?.navBarBgColor ?? UIColor.GGGreen
        self.headerView.backgroundColor = bgColor
    }
}
