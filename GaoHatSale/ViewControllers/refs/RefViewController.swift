//
//  RefViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/1/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

class RefViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPhone: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var btRef: UIButton!
    @IBOutlet weak var vContent: UIView!
    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    
    var keyboardHandler : KeyboardHandler!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        keyboardHandler = KeyboardHandler(view: self.view, bottomConstr: bottomScrollConstraint)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.dismissKeyboardOnTap()
    }
    @IBAction func onRefClick(_ sender: UIButton) {
        if Utils.isEmpty(userName.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập họ tên người được giới thiệu.")
            return
        }
        else if Utils.isEmpty(userPhone.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập số điện thoại của người được giới thiệu.")
            return
        }
        else if Utils.isEmpty(userEmail.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập email của người được giới thiệu.")
            return
        }
        else if !Utils.isValidEmail(userEmail.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Kiểm tra lại cú pháp email.")
            return
        }
        else {
            GHAPI.api.ref(fullName: userName.text ?? "", phone: userPhone.text ?? "", email: userEmail.text ?? "").done {
                Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Giới thiệu thành công.")
                self.resetView()
            }.catch {
                err in
                Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Đã có lỗi xảy ra.")
            }
        }
    }
    
    func resetView() {
        userName.text = ""
        userPhone.text = ""
        userEmail.text = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardHandler.attachListener()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        keyboardHandler.removeListener()
    }
}
