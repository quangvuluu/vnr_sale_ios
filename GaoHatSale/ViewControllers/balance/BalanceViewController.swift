//
//  AccountViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/1/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import PullToRefreshKit

class BalanceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lbSellReward: UILabel!
    @IBOutlet weak var lbRefReward: UILabel!
    @IBOutlet weak var lbLeadReward: UILabel!
    @IBOutlet weak var lbBalance: UILabel!
    @IBOutlet weak var tbTransaction: UITableView!
    @IBOutlet weak var btnUpdateBank: UIButton!
    @IBOutlet weak var btnCashout: UIButton!
    
    var balance: Balance?
    
    var trans : Array<Transaction> = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTable()
        
        GHAPI.api.getBalance().done { balance in
            self.setViewBalance(balance: balance)
        }
        
        GHAPI.api.getTransactions("").done { transactions in
            self.trans = transactions
            self.tbTransaction.reloadData()
        }
        
        tbTransaction.configRefreshFooter(container: self) {
            self.loadMore()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupUser()
    }
    
    func setupUser() {
        let user = Glob.user
        self.btnCashout.isHidden = (user.bankAccount == nil)
    }
    
    func loadMore() {
        if trans.isEmpty {
            self.tbTransaction.endFooterRefreshing()
            return
        }
        GHAPI.api.getTransactions(trans[trans.count - 1]._id).done { newTrans in
            if !newTrans.isEmpty {
                self.trans.append(contentsOf : newTrans)
                self.tbTransaction.reloadData()
                self.tbTransaction.endFooterRefreshing()
            }
            else {
                self.tbTransaction.switchRefreshFooter(to: .noMoreData)
            }
        }.catch {err in
                self.tbTransaction.endFooterRefreshing()
        }
    }

    func setupTable() {
        tbTransaction.estimatedRowHeight = 80
        tbTransaction.register(UINib(nibName: "TransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "transactionCell")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if trans.isEmpty {
            tableView.showEmptyMessage(message: "Chưa có dữ liệu giao dịch")
        }
        else {
            tableView.backgroundView = nil
        }
        return trans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath) as! TransactionTableViewCell
        
        let item = trans[indexPath.row]
        cell.setUpCell(item)
        
        return cell
    }
    
    func setViewBalance(balance : Balance) {
        self.balance = balance
        lbBalance.text = "\(balance.balance.formattedWithSeparator)đ"
        lbBalance.textColor = balance.balance >= 0 ? UIColor.GGGreen : UIColor.GGRed
        
        if (balance.report["REF_REWARD"] != nil) {
            lbRefReward.text = "Giới thiệu : \((balance.report["REF_REWARD"] ?? 0).formattedWithSeparator)đ"
        } else {
            lbRefReward.isHidden = true
        }
        
        if (balance.report["LEAD_REWARD"] != nil) {
            lbLeadReward.text = "Trưởng nhóm : \((balance.report["LEAD_REWARD"] ?? 0).formattedWithSeparator)đ"
        } else {
            lbLeadReward.isHidden = true
        }
        
        if (balance.report["SELL_REWARD"] != nil) {
            lbSellReward.text = "Bán hàng : \((balance.report["SELL_REWARD"] ?? 0).formattedWithSeparator)đ"
        }
        else {
            lbSellReward.isHidden = true
        }
    }
    
    @IBAction func btnUpdateBankAccountPressed(_ sender: Any) {
        self.navigationController?.pushViewController(UpdateBankAccountViewController(nibName: "UpdateBankAccountViewController", bundle: nil), animated: true)
    }
    
    @IBAction func btnCashoutPressed(_ sender: Any) {
        if self.balance == nil {
            return
        }
        
        let balance = self.balance!
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Rút tiền", message: "", preferredStyle: .alert)

        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Số tiền muốn rút..."
        }

        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            if let amount = Int(textField?.text ?? "") {
                if (amount <= 0) {
                    return
                }
                if (amount < 300000) {
                    Utils.showOKAlertPanel(self, title: "Lỗi", msg: "Một lần rút tiền tối thiểu 300,000")
                }
                if (amount < balance.balance) {
                    Utils.showOKAlertPanel(self, title: "Lỗi", msg: "Bạn không đủ tiền")
                }
                
                _ = GHAPI.api.newCashoutReq(amount: amount).done({
                    balance.balance -= amount
                    self.setViewBalance(balance: balance)
                }).catch({ err in
                    Utils.showOKAlertPanel(self, title: "Lỗi", msg: "Không thể rút tiền. Xin vui lòng thử lại")
                })
            }
        }))

        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
}
