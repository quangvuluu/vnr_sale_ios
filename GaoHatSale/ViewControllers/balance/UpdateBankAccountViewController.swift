//
//  UpdateBankAccountViewController.swift
//  GaoHatSale
//
//  Created by Vu Luu Quang on 9/10/20.
//  Copyright © 2020 kieuanh Nguyen. All rights reserved.
//

import UIKit

class UpdateBankAccountViewController: UIViewController, NavBarDelegate {

    @IBOutlet weak var btScrollView: NSLayoutConstraint!
    
    @IBOutlet weak var tfBankName: UITextField!
    @IBOutlet weak var tfAccountNo: UITextField!
    @IBOutlet weak var tfHolderName: UITextField!
    @IBOutlet weak var tfBranch: UITextField!
    @IBOutlet weak var tfNationalCode: UITextField!
    
    var user: User!
    
    var keyboardHandler: KeyboardHandler!
    
    var navBarTitle: String {
        return "TÀI KHOẢN NGÂN HÀNG"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.dismissKeyboardOnTap()
        keyboardHandler = KeyboardHandler(view: self.view, bottomConstr: self.btScrollView)
        
        self.user = Glob.user
        
        self.tfBankName.text = user.bankAccount?.bank
        self.tfAccountNo.text = user.bankAccount?.accountNo
        self.tfHolderName.text = user.bankAccount?.holderName
        self.tfBankName.text = user.bankAccount?.branch
        self.tfNationalCode.text = user.nationalCode
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.keyboardHandler.attachListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.keyboardHandler.removeListener()
        self.view.endEditing(true)
    }
    
    @IBAction func btnUpdatePressed(_ sender: Any) {
        if Utils.isEmpty(self.tfBankName.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập tên ngân hàng.")
            return
        }
        if Utils.isEmpty(self.tfAccountNo.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập số tài khoản.")
            return
        }
        if Utils.isEmpty(self.tfHolderName.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập tên chủ tài khoản.")
            return
        }
        if Utils.isEmpty(self.tfBranch.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập tên chi nhánh.")
            return
        }
        if Utils.isEmpty(self.tfNationalCode.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập số CMND.")
            return
        }
        
        let account = UserBankAccount()
        account.bank = tfBankName.text ?? ""
        account.accountNo = tfAccountNo.text ?? ""
        account.holderName = tfHolderName.text ?? ""
        account.branch = tfBranch.text ?? ""
        
        _ = GHAPI.api.updateBankAccount(bankAccount: account, nationalCode: tfNationalCode.text!).done { [weak self] in
            if self == nil {
                return
            }
            
            self?.user.bankAccount = account
            self?.user.nationalCode = self?.tfNationalCode.text ?? ""
            
            Utils.showOKAlertPanel(self!, title: "Thông báo", msg: "Cập nhật thông tin tài khoản thành công.")
            self?.navigationController?.popViewController(animated: true)
        }.catch { err in
            Utils.showOKAlertPanel(self, title: "Lỗi", msg: "Không thể cập nhật thông tin tài khoản. Xin vui lòng thử lại.")
        }
    }
}
