//
//  TransactionTableViewCell.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/5/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var vContent: UIView!
    @IBOutlet weak var OrderContent: UILabel!
    @IBOutlet weak var OrderName: UILabel!
    @IBOutlet weak var OrderTime: UILabel!
    @IBOutlet weak var OrderTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.vContent.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUpCell(_ transaction : Transaction) {
        OrderName.text = transaction.meta_data
        OrderTime.text = DateFormat.defaultDisplayFormatter.string(from: transaction.create_at)
        OrderContent.text = transaction.content
        OrderTotal.text = "\(transaction.amount.formattedWithSeparator)đ"
        
        if transaction.amount < 0 {
            OrderTotal.textColor = UIColor.GGRed
        }
        else {
            OrderTotal.textColor = UIColor.GGGreen
        }
    }
}
