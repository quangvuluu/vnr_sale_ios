//
//  RegisterViewController.swift
//  GaoHatSale
//
//  Created by Vu Luu Quang on 9/18/20.
//  Copyright © 2020 kieuanh Nguyen. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    let prevVC: UIViewController
    
    init(prevVC: UIViewController) {
        self.prevVC = prevVC
        super.init(nibName: "RegisterViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfPasswordConfirm: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var keyboardHandler: KeyboardHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.dismissKeyboardOnTap()
        keyboardHandler = KeyboardHandler(view: self.view, bottomConstr: self.bottomConstraint)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.keyboardHandler.attachListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.keyboardHandler.removeListener()
        self.view.endEditing(true)
    }
    
    @IBAction func btnRegisterPressed(_ sender: Any) {
        if Utils.isEmpty(self.tfName.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập họ tên.")
            return
        }
        if Utils.isEmpty(self.tfPhone.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập số điện thoại.")
            return
        }
        
        if Utils.isEmpty(self.tfPassword.text) {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Bạn chưa nhập mật khẩu.")
            return
        }
        if self.tfPasswordConfirm.text != self.tfPassword.text {
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Mật khẩu nhập lại không khớp.")
            return
        }
        
        _ = GHAPI.api.register(name: tfName.text!, phone: tfPhone.text!, password: tfPassword.text!).done { [weak self] in
            if self == nil {
                return
            }
            
            Utils.showOKAlertPanel(self!, title: "Thông báo", msg: "Cập nhật thông tin tài khoản thành công.") { _ in
                self?.btnBackPressed(sender)
            }
        }.catch({ err in
            Utils.showOKAlertPanel(self, title: "Lỗi", msg: "Không thể đăng ký. Xin vui lòng thử lại.")
        })
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        let window: UIWindow! = UIApplication.shared.delegate?.window!
        window.rootViewController = prevVC
        window.makeKeyAndVisible()
    }
}
