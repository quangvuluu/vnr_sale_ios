//
//  SaleViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/1/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import iOSDropDown
import GooglePlaces
import GoogleMaps
import INTULocationManager
import GooglePlacesSearchController
import MapKit
import CoreLocation
import FontAwesome_swift

class SaleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, OnClearProductClicked, UITextFieldDelegate {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userAddress: UITextField!
    @IBOutlet weak var btnSearchAddress: UIButton!
    @IBOutlet weak var userPhone: UITextField!
    @IBOutlet weak var btnSearchPhone: UIButton!
    @IBOutlet weak var userNoFamilyMembers: UITextField!
    @IBOutlet weak var note: UITextField!
    @IBOutlet weak var tbProducts: UITableView!
    @IBOutlet weak var viewSum: UIView!
    @IBOutlet weak var lbSum: UILabel!
    @IBOutlet weak var btAddOrder: UIButton!
    @IBOutlet weak var lbAmount: UITextField!
    @IBOutlet weak var riceType: DropDown!
    @IBOutlet weak var btAddProduct: UIButton!
    @IBOutlet weak var vContent: UIView!
    @IBOutlet weak var bottomScrollConstraint: NSLayoutConstraint!
    @IBOutlet weak var tbProductHeight: NSLayoutConstraint!
    
    var keyboardHandler : KeyboardHandler!
    
    let GoogleMapsAPIServerKey = "AIzaSyCPz6bbNTXUiDfDVXMV7MGV6HJuSOt83I4"
    lazy var placesSearchController : GooglePlacesSearchController = {
        let controller = GooglePlacesSearchController(delegate : self,
                                                      apiKey : GoogleMapsAPIServerKey,
                                                      placeType : .address,
                                                      coordinate: CLLocationCoordinate2D(latitude: 10.48, longitude: 106.39),
            radius: 30,
            searchBarPlaceholder: "Nhập địa chỉ...")
        return controller
    }()
    
    var orderRequest : OrderRequest = OrderRequest()
    var products : Array<AmountRequest> = []
    var allProduct : Array<Product> = []
    var productSelectedCode = ""
    var user = User()
    var userPlace = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        keyboardHandler = KeyboardHandler(view: self.view, bottomConstr: bottomScrollConstraint)

        self.setUpTable()
        btAddProduct.layer.cornerRadius = 5
        
        btnSearchAddress.titleLabel?.font = UIFont.fontAwesome(ofSize: 24, style: .solid)
        btnSearchAddress.setTitle(String.fontAwesomeIcon(name: .search), for: .normal)
        btnSearchAddress.layer.cornerRadius = 5
        
        btnSearchPhone.titleLabel?.font = UIFont.fontAwesome(ofSize: 24, style: .solid)
        btnSearchPhone.setTitle(String.fontAwesomeIcon(name: .search), for: .normal)
        btnSearchPhone.layer.cornerRadius = 5
        
        self.allProduct = Glob.allProducts
        self.user = Glob.user
        
        self.vContent.dismissKeyboardOnTap(self.view, cancelTapInViews: false)
        
        if products.count == 0 {
            viewSum.isHidden = true
        }
        
        self.riceType.optionArray = self.allProduct.map { $0.name }
        self.riceType.hideOptionsWhenSelect = true
        self.riceType.didSelect { (text, idx, id) in
            self.productSelectedCode = self.allProduct[idx].code
        }
        
    }
    
    func setUpTable() {
        tbProducts.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "productCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! ProductTableViewCell
        
        let item = products[indexPath.row]
        cell.listener = self
        cell.setUpCell(product: item, pos : indexPath.row)
        return cell
    }
    
    @IBAction func onAddProduct(_ sender: Any) {
        if productSelectedCode == "" {
            toast(mess: "Bạn chưa nhập loại gạo.")
            return
        }
        else if Utils.isEmpty(lbAmount.text) {
            toast(mess: "Bạn chưa nhập số lượng gạo.")
            return
        }
    
        riceType.hideList()
        let pro = Glob.allProducts.first(where: { p -> Bool in
            p.code == productSelectedCode
        })!
        
        let price = pro.price * (Int(lbAmount.text!) ?? 0)
        let amount =  AmountRequest(product : productSelectedCode, amount : Int(lbAmount.text!) ?? 0, price : price)
        
        products.append(amount)
        
        tbProducts.reloadData()
        
        updateViewSum()
    }
    
    @IBAction func onAddOrder(_ sender: Any) {
        if self.checkData() {
            var receiver = orderRequest.receiver
            receiver.address = userAddress.text ?? ""
            receiver.lat = userPlace.latitude ?? 0.0
            receiver.long = userPlace.longitude ?? 0.0
            receiver.name = userName.text ?? ""
            receiver.phone = userPhone.text ?? ""
            receiver.area = self.user.area
            receiver.consumtion_rate = Int(userNoFamilyMembers.text!) ?? 0
            
            orderRequest.order.amount = products
            orderRequest.order.note = note.text ?? ""
            
            GHAPI.api.addOrder(order: self.orderRequest).done {
                    self.toast(mess: "Thêm đơn hàng thành công.")
                }.catch {
                    err in
                    self.toast(mess: "Đã có lỗi xảy ra.")
                    self.resetView()
            }
        }
    }
    
    func resetView() {
        userName.text = ""
        userPhone.text = ""
        userAddress.text = ""
        userNoFamilyMembers.text = ""
        note.text = ""
        lbAmount.text = ""
    }
    
    func checkData() -> Bool {
        if Utils.isEmpty(userName.text) {
            toast(mess: "Bạn chưa nhập tên khách hàng.")
            return false
        }
        else if Utils.isEmpty(userPhone.text) {
            toast(mess: "Bạn chưa nhập số điện thoại khách hàng.")
            return false
        }
        else if Utils.isEmpty(userAddress.text) {
            toast(mess: "Bạn chưa nhập địa chỉ khách hàng.")
            return false
        }
        
        return true
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tbProductHeight.constant = self.tbProducts.contentSize.height
    }
    
    func onClearProductClick(pos : Int) {
        self.products.remove(at : pos)
        tbProducts.reloadData()
        self.updateViewSum()
        vContent.needsUpdateConstraints()
    }
    
    func updateViewSum() {
        if products.count != 0 {
            viewSum.isHidden = false
            var sum = 0
            for i in self.products {
                sum += i.price
            }
            lbSum.text = "\(sum.formattedWithSeparator)đ"
        }
        else {
            viewSum.isHidden = true
        }
    }
    
    func onAddressClick(_ sender: Any) {
        INTULocationManager.sharedInstance().requestLocation(withDesiredAccuracy: .block, timeout: 10, delayUntilAuthorized: false) { loc, _, status in
            if (status == .success) {
                self.userPlace.latitude = loc!.coordinate.latitude
                self.userPlace.longitude = loc!.coordinate.longitude
            }
            
            self.present(self.placesSearchController, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSearchAddressPressed(_ sender: Any) {
        self.view.endEditing(true)
        self.onAddressClick(sender)
    }
    
    @IBAction func btnSearchPhonePressed(_ sender: Any) {
        self.view.endEditing(true)
        if Utils.isEmpty(self.userPhone.text) { return }
        
        let phone = self.userPhone.text ?? ""
        GHAPI.api.searchCustomerPhone(phone: phone).done { (customer) in
            if let customer = customer {
                self.userName.text = customer.fullName
                self.userAddress.text = customer.address
                self.userPlace.latitude = customer.lat
                self.userPlace.longitude = customer.long
                self.note.text = customer.note
            }
            else {
                throw NSError(domain: "User not found", code: 0, userInfo: nil)
            }
        }.catch { (err) in
            Utils.showOKAlertPanel(self, title: "Không tìm thấy", msg: "Số điện thoại không tồn tại. Xin vui lòng thử lại.")
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.userAddress {
            if textField.text?.isEmpty ?? true {
                self.view.endEditing(true)
                self.onAddressClick(textField)
            }
        }
    }
}

protocol OnClearProductClicked{
    func onClearProductClick(pos : Int)
}

extension SaleViewController : GooglePlacesAutocompleteViewControllerDelegate {
    func viewController(didAutocompleteWith place: PlaceDetails) {
        userAddress.text = place.formattedAddress
        self.userPlace = place.coordinate!
        placesSearchController.isActive = false
    }
    
    func toast(mess : String) {
        Utils.showOKAlertPanel(self, title: "Thông báo", msg: mess)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        placesSearchController.isActive = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        keyboardHandler.attachListener()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        keyboardHandler.removeListener()
    }
}
