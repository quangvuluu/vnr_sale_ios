//
//  ProductTableViewCell.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/9/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productAmount: UILabel!
    
    var listener : OnClearProductClicked?
    var pos : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUpCell(product : AmountRequest, pos : Int) {
        let pro = Glob.allProducts.first(where: { p -> Bool in
            p.code == product.product
        }) as! Product
        
        productName.text = pro.name
        productAmount.text  = "\(product.amount)kg"
        
        self.pos = pos
    }
    
    @IBAction func clearProduct(_ sender: Any) {
        listener?.onClearProductClick(pos : pos)
    }
}
