//
//  LoadingViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/20/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let window: UIWindow! = UIApplication.shared.delegate?.window!
        
        if self.isReachable {
            let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            GHAPI.api.checkVersion(version: version).done { ver in
                if (ver.success) {
                    window.rootViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
                    window.makeKeyAndVisible()
                }
                else {
                    Utils.showOKAlertPanel(self, title: "Lỗi phiên bản", msg: "Vui lòng cập nhật bản mới nhất")
                    
                    if let url = URL(string: ver.url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) ?? "") {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }
                }
            }.catch { err in
                Utils.showOKAlertPanel(self, title: "Lỗi phiên bản", msg: "Vui lòng cập nhật bản mới nhất")
            }
        }
        else {
            window.rootViewController = OfflineViewController(nibName: "OfflineViewController", bundle: nil)
            window.makeKeyAndVisible()
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
