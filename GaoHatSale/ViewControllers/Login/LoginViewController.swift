//
//  LoginViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/1/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var btLogin: UIButton!
    @IBOutlet weak var btScrollview: NSLayoutConstraint!
    
    var KEY_ACCESS_TOKEN : String = "access_token"
     var KEY_REFRESH_TOKEN : String = "refresh_token"
    var KEY_SCOPE : String = "scope"
    
    var keyboardHandler: KeyboardHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btLogin.layer.cornerRadius = 5
        if !Utils.isEmpty(UserDefaults.standard.string(forKey: KEY_ACCESS_TOKEN)) {
            self.openMain()
            Glob.accessToken = UserDefaults.standard.string(forKey: KEY_ACCESS_TOKEN)
        } else {
            self.btLogin.layer.cornerRadius = 5
            view.dismissKeyboardOnTap()
            
            keyboardHandler = KeyboardHandler(view: self.view, bottomConstr: self.btScrollview)
            
//            self.tfUserName.text = "01663016163"
//            self.tfPassword.text = "123456789"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        keyboardHandler.attachListener()
    }
    
    @IBAction func onLoginClick(_ sender: UIButton) {
        // TODO: check empty
        
        let userName = self.tfUserName.text!
        let password = self.tfPassword.text!
        
        _ = GHAPI.api.login(username: userName, password: password).done { auth in
            Glob.accessToken = auth.access_token
            
            UserDefaults.standard.set(auth.access_token, forKey: self.KEY_ACCESS_TOKEN)
            UserDefaults.standard.set(auth.refresh_token, forKey: self.KEY_REFRESH_TOKEN)
            UserDefaults.standard.set(auth.scope, forKey: self.KEY_SCOPE)
            
            self.openMain()
            
        }.catch { err in
            Utils.showOKAlertPanel(self, title: "Không đăng nhập được", msg: "Sai tài khoản hoặc mật khẩu. Xin vui lòng thử lại")
        }
    }
    
    func openMain() {
        let appNavVC = AppNavViewController()
        let window = UIApplication.shared.delegate?.window!
        window?.rootViewController = appNavVC
        window?.makeKeyAndVisible()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        keyboardHandler.removeListener()
    }
    
    @IBAction func btnRegPressed(_ sender: Any) {
        let window: UIWindow! = UIApplication.shared.delegate?.window!
        let regVC = RegisterViewController(prevVC: self)
        window.rootViewController = regVC
        window.makeKeyAndVisible()
    }
    
    var envCount = 0
    
    @IBAction func btnEnvPressed(_ sender: Any) {
        envCount += 1
        if envCount >= 5 {
            ENV = ENV == .Production ? .Staging : .Production
            let txt = ENV == .Production ? "Production" : "Staging"
            envCount = 0
            GHAPI.api.reloadConfig()
            Utils.showOKAlertPanel(self, title: "Thông báo", msg: "Môi trường \(txt)")
        }
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        Utils.showYesNoInputAlertPanel(self, title: "Quên mật khẩu", msg: "Nhập thông tin tài khoản:", placeHolder: "Nhập email hoặc SĐT", yes: "Gửi", no: "Huỷ") { (isYes, info) in
            if !isYes || Utils.isEmpty(info) { return }
            _ = GHAPI.api.forgotPassword(info: info).done {
                Utils.showOKAlertPanel(self, title: "Gửi yêu cầu thành công", msg: "Hệ thống sẽ gửi email cho bạn trong thời gian sớm nhất")
            }
        }
    }
}
