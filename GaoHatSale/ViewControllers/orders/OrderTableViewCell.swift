//
//  OrderTableViewCell.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/5/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var vContent: UIView!
    @IBOutlet weak var orderStatus: UILabel!
    @IBOutlet weak var lbOrderName: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var orderTime: UILabel!
    @IBOutlet weak var lbSum: UILabel!
    @IBOutlet weak var lbproducts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.vContent.layer.cornerRadius = 10
        self.orderStatus.layer.cornerRadius = 5
        self.orderStatus.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setUpCell(_ order : Order) {
        lbOrderName.text = "ĐƠN HÀNG : \(order.code)"
        userName.text = order.customer?.fullName
        userPhone.text = ""
        orderTime.text = DateFormat.defaultDisplayFormatter.string(from: order.created_at)
        
        var products = ""
        for pr in order.amount {
            for i in Glob.allProducts {
                if i.code == pr.product {
                    products += "\(i.name)    \(pr.amount)kg \n"
                }
            }
        }
        
        lbSum.text = "\(order.total_price.formattedWithSeparator)đ"
        lbproducts.numberOfLines = order.amount.count
        lbproducts.text = products
        
        orderStatus.backgroundColor = order.statusColor
        orderStatus.text = order.statusDescription
    }
}
