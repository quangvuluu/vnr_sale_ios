//
//  OrdersViewController.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/2/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import PullToRefreshKit

class OrdersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tbOrders: UITableView!
    
    var orders : Array<Order> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTable()
        GHAPI.api.getOrders(id : "").done { orders in
            self.orders = orders
            self.tbOrders.reloadData()
        }
        
        tbOrders.configRefreshFooter(container: self) {
            self.loadMore()
        }
    }
    
    func setupTable() {
        tbOrders.estimatedRowHeight = 180
        tbOrders.register(UINib(nibName: "OrderTableViewCell", bundle: nil), forCellReuseIdentifier: "orderCell")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orders.isEmpty {
            tableView.showEmptyMessage(message: "Chưa có dữ liệu đơn hàng")
        }
        else {
            tableView.backgroundView = nil
        }
        return orders.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell", for: indexPath) as! OrderTableViewCell
        
        let item = orders[indexPath.row]
        cell.setUpCell(item)
        
        cell.contentView.updateConstraintsIfNeeded()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let vc = OrderDetailViewController(order: self.orders[indexPath.row])
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loadMore() {
        if orders.isEmpty {
            tbOrders.endFooterRefreshing()
            return
        }
        GHAPI.api.getOrders(id : orders[orders.count - 1]._id).done { newOrders in
            if !newOrders.isEmpty {
                self.orders.append(contentsOf : newOrders)
                self.tbOrders.reloadData()
                self.tbOrders.endFooterRefreshing()
            }
            else {
                self.tbOrders.switchRefreshFooter(to: .noMoreData)
            }
            }.catch {err in
                self.tbOrders.endFooterRefreshing()
        }
    }
}
