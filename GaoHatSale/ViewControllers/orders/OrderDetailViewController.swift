//
//  OrderDetailViewController.swift
//  VNRShip
//
//  Created by Vu Luu Quang on 9/6/20.
//  Copyright © 2020 concon. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController, NavBarDelegate {
    init(order: Order) {
        self.order = order
        super.init(nibName: "OrderDetailViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let order: Order
    
    var navBarTitle: String {
        return "ĐƠN HÀNG \(self.order.code)"
    }
    
    @IBOutlet weak var lbCustomerName: UILabel!
    @IBOutlet weak var lbCustomerAddress: UILabel!
    @IBOutlet weak var lbProducts: UILabel!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var lbNote: UILabel!
    @IBOutlet weak var lbExpired: UILabel!
    @IBOutlet weak var lbCancellingReason: UILabel!
    @IBOutlet weak var vCancellingReason: UIView!
    @IBOutlet weak var lbStoreDesc: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    func setupView() {
        self.lbCustomerName?.text = order.customer?.fullName
        self.lbCustomerAddress?.text = order.customer?.address
        self.lbProducts?.text = order.productDescription
        self.lbTotal?.text = "\(order.total_price.formattedWithSeparator) đ"
        self.lbNote?.text = order.note
        self.lbStatus?.text = order.statusDescription
        self.lbStatus?.textColor = order.statusColor
        self.lbExpired?.text = DateFormat.dateTimeFormatter.string(from: order.expired_at)
        self.lbStoreDesc?.text = order.deliverer?.storeDescription ?? "Không xác định"
        self.btnCancel?.isHidden = order.status != "DELIVERING"
        
        if let reason = order.cancelling_reason {
            self.lbCancellingReason?.text = reason
        }
        else {
            self.vCancellingReason?.removeFromSuperview()
        }
    }
    
    var isPhoneAvailable: Bool {
        return self.order.status == "DELIVERING" || self.order.status == "TIMED_OUT"
    }
    
    @IBAction func btnCancelPressed(_ sender: Any) {
        Utils.showYesNoAlertPanel(self, title: "Huỷ đơn hàng", msg: "Bạn có chắc chắn muốn huỷ đơn hàng này ?", yes: "Đồng ý", no: "Không") { isYes in
            
            if !isYes {return}
            GHAPI.api.cancelOrder(orderId: self.order._id).done { [self] in
                Utils.showOKAlertPanel(self, title: "Huỷ đơn hàng", msg: "Đã huỷ đơn hàng thành công")
                order.status = "CANCELLED"
                Utils.later {
                    self.setupView()
                }
            }.catch {_ in
                Utils.showOKAlertPanel(self, title: "Lỗi", msg: "Không thể huỷ đơn hàng. Xin vui lòng kiểm tra và thử lại")
            }
        }
    }
    
}
