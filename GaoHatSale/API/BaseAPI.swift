//
//  API.swift
//  DoctorApp
//
//  Created by Salm on 4/27/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import ObjectMapper
import PromiseKit

typealias APIMethod = Alamofire.HTTPMethod
typealias APIData = JSON

extension Promise {
    static func resolve(_ obj: T) -> Promise<T> {
        return Promise<T> { $0.fulfill(obj) }
    }
    
    static func reject<T>(_ err: Error? = nil) -> Promise<T> {
        return Promise<T>(error: err ?? NSError())
    }
    
    func query(_ keyPath: String) -> Promise<APIData> {
        return self.mapPromise { data in
            if let apiData = data as? APIData {
                let subData: APIData = apiData.query(keyPath)
                return Promise<APIData>.resolve(subData)
            }
            
            return Promise.reject()
        }
    }
    
    func asObject<O: Mappable>() -> Promise<O> {
        let mapper = Mapper<O>()
        return self.mapPromise({ data in
            if let apiData = data as? APIData {
                if let obj: O = mapper.map(JSON: apiData.dictionaryObject ?? [:]) {
                    return Promise<O>.resolve(obj)
                }
            }
            
            return Promise.reject()
        })
    }
    
    func asOptionalObject<O: Mappable>() -> Promise<O?> {
        let mapper = Mapper<O>()
        return self.then { (data) -> Promise<O?> in
            if let apiData = data as? APIData {
                let obj: O? = mapper.map(JSON: apiData.dictionaryObject ?? [:])
                return Promise<O?>.resolve(obj)
            }
            
            return Promise<O?>.resolve(nil)
        }
    }
    
    func asArr<O: Mappable>() -> Promise<[O]> {
        let mapper = Mapper<O>()
        return self.mapPromise({ data in
            if let apiData = data as? APIData {
                let arr: [O] = mapper.mapArray(JSONArray: apiData.safeArrayObject) ?? []
                return Promise<[O]>.resolve(arr)
            }
            
            return Promise.reject()
        })
    }
    
    func asDict<O: Mappable>() -> Promise<[String: O]> {
        let mapper = Mapper<O>()
        return self.mapPromise({ data in
            if let apiData = data as? APIData {
                let dict: [String: O] = mapper.mapDictionary(JSON: apiData.safeDictObject) ?? [:]
                return Promise<[String: O]>.resolve(dict)
            }
            
            return Promise.reject()
        })
    }
    
    func asArr<O>(_ transformer: @escaping (APIData) -> O) -> Promise<[O]> {
        return self.mapPromise({ data in
            if let apiData = data as? APIData {
                let arr: [O] = apiData.arrayValue.map(transformer)
                return Promise<[O]>.resolve(arr)
            }
            
            return Promise.reject()
        })
    }
    
    func asDict<O>(_ transformer: @escaping (APIData) -> O) -> Promise<[String: O]> {
        return self.mapPromise({ data in
            if let apiData = data as? APIData {
                let dict = apiData.dictionaryValue
                var ret: [String: O] = [:]
                for (key, val) in dict {
                    ret[key] = transformer(val)
                }
                
                return Promise<[String: O]>.resolve(ret)
            }
            
            return Promise.reject()
        })
    }
    
    func cast<T>() -> Promise<T?> {
        return self.map({ data in
            return data as? T
        })
    }
}

open class API {
    // define base API method
    public struct Config {
        let schema: String
        let host: String
        let port: Int
        let basePath: String
        
        init(schema: String = "http", host: String = "localhost", basePath: String = "", port: Int = 80) {
            self.schema = schema
            self.host = host
            self.port = port
            self.basePath = basePath
        }
        
        lazy var baseUrl: String = {
            var portDesc = (self.port == 80) ? "" : ":\(self.port)"
            return "\(self.schema)://\(self.host)\(portDesc)\(self.basePath)"
        }()
    }
    
    open class RequestConfig {
        var method: APIMethod = .get
        var headers: HTTPHeaders = HTTPHeaders()
        var url: String = ""
        var params: APIData?
    }
    
    open class APIError: Error {
        var code: Int = -1
        var msg: String = "Unkown error"
        var params: [String] = []
    }
    
    open class ResponseConfig {
        var error: NSError?
        var data: APIData?
        var status: Int = 200
        var apiError: APIError?
    }
    
    public typealias RequestInterceptor = (_ req: RequestConfig) -> Void
    public typealias ResponseInterceptor = (_ resp: ResponseConfig) -> Void
    
    open var config = Config()
    open var requestInterceptors: [RequestInterceptor] = []
    open var responseInterceptors: [ResponseInterceptor] = []
    
    func url(_ path: String) -> String {
        var _path = path
        if (!path.hasPrefix("/")) {
            _path = "/\(path)"
        }
        
        return "\(self.config.baseUrl)\(_path)"
    }
    
    func baseAPI(_ method: APIMethod,
                 path: String, body: APIData?) -> Promise<APIData> {
        let reqConfig = RequestConfig()
        reqConfig.params = body
        reqConfig.url = url(path)
        reqConfig.method = method
        self.requestInterceptors.forEach() {$0(reqConfig)}
        
        return Promise<APIData>(resolver: { resolve in
            AF.request(reqConfig.url, method: reqConfig.method, parameters: reqConfig.params?.dictionaryObject, encoding: JSONEncoding.default, headers: reqConfig.headers)
                .responseJSON { resp in
                    let respConfig: ResponseConfig = ResponseConfig()
                    switch resp.result {
                    case .success(let respData):
                        let data = APIData(respData)
                        respConfig.data = data
                        respConfig.status = resp.response?.statusCode ?? 500
                        break
                    case .failure(let error):
                        let data = APIData(resp.data!)
                        respConfig.data = data
                        respConfig.error = error as NSError?
                        respConfig.status = resp.response?.statusCode ?? 50
                        break
                    }
                    
                    self.responseInterceptors.forEach() {$0(respConfig)}
                    if let apiErr = respConfig.apiError {
                        resolve.reject(apiErr)
                    }
                    else {
                        resolve.fulfill(respConfig.data ?? [:])
                    }
            }
        })
    }
}
