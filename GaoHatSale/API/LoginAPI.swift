//
//  LoginAPI.swift
//  DLive
//
//  Created by Salm on 6/26/17.
//  Copyright © 2017 Salm. All rights reserved.
//

import UIKit
import PromiseKit
import ObjectMapper

class AuthToken: Mappable {
    var access_token: String = ""
    var expires_in: Double = 0
    var refresh_token: String = ""
    var token_type: String = ""
    var scope: String = ""
    
    required init?(map: Map) {
        if !NSObject.validField(AuthToken.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        access_token <- map["access_token"]
        expires_in <- map["expires_in"]
        refresh_token <- map["refresh_token"]
        token_type <- map["token_type"]
        scope <- map["scope"]
    }
}

extension GHAPI {
    func login(username: String, password: String) -> Promise<AuthToken> {
        return self.baseAPI(.post, path: "/auth/login", body: ["phone": username, "password": password, "roles": ["ASM", "SALE"]]).asObject()
    }

//    func issueToken(refreshToken: String) -> Promise<LoginResponse> {
//        return self.baseAPI(.post, path: "/login/token", body: ["refresh": refreshToken]).asObject()
//    }
}
