
//  CarnaviAPI.swift
//  iqx
//
//  Created by Salm on 8/27/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit

class GHAPI: API {
    
    override init() {
        super.init()
        self.reloadConfig()
        self.requestInterceptors.append(self.requestIntercept)
        self.responseInterceptors.append(self.responseIntercept)
    }
    
    func reloadConfig() {
        self.config = API.Config(schema: CONFIG.API_SCHEMA, host: CONFIG.API_URL, basePath: "", port: CONFIG.API_PORT)
    }
    
    func requestIntercept(_ req: API.RequestConfig) {
        if let loginToken = Glob.accessToken {
            req.headers = [
                "authorization": loginToken
            ]
        }
        
        debugPrint("Request: \(req.url)")
    }
    
    func responseIntercept(_ resp: API.ResponseConfig) {
        let err: NSError? = resp.error
        if err != nil || resp.status >= 300 {
            let _err = resp.data?["err"]
            
            let apiErr = APIError()
            apiErr.code = _err?["code"].safeInt ?? err?.code ?? -1
            apiErr.msg = _err?["msg"].safeString ?? _err?["message"].safeString ?? err?.localizedDescription ?? "Unknown error"
            apiErr.params = _err?["params"].safeArray({$0.safeString}) ?? []
            resp.apiError = apiErr
        }
        
        debugPrint("Response:")
        print(resp.data ?? APIData.null)
    }
    
    static let api = GHAPI()
}
