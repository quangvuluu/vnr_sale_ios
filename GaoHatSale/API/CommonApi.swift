//
//  CommonApi.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/7/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import PromiseKit

extension GHAPI {
    func getProducts() -> Promise<Array<Product>>  {
        return self.baseAPI(.get, path: "products?$fields=code,name,description,unit,price", body: nil).asArr()
    }
    
    func getBalance() -> Promise<Balance> {
        return self.baseAPI(.get, path: "/balances/me?$fields=*", body: nil).asObject()    }
    
    func getTransactions(_ id : String) -> Promise<Array<Transaction>> {
        var path = "/balances/me/transactions?$fields=_id,amount,type,created_at,meta_data,content&$sort=created_at:DESC&$limit=10"
        
        if !Utils.isEmpty(id) {
            path.append("&$to=_id:\(id)")
        }
        
        return self.baseAPI(.get, path: path, body: nil).asArr()
    }
    
    func getProfile() ->  Promise<User> {
        return self.baseAPI(.get, path: "/users/me?$fields=_id,phone,fullName,avatar,ext,roles,area,location,bankAccount", body: nil).asObject()
    }

    func changePass(oldPassword : String, newPassword : String) -> Promise<Void> {
        return self.baseAPI(.put, path: "/users/me/password", body: ["oldPassword": oldPassword, "newPassword": newPassword]).asVoid()
    }
    
    func getOrders(id : String) -> Promise<Array<Order>> {
        var path = "orders?$fields=*,customer.*,deliverer.storeDescription&$sort=_id:DESC&$limit=10"
        if !Utils.isEmpty(id) {
            path.append("&$to=_id:\(id)")
        }
        
        return self.baseAPI(.get, path: path, body: nil).asArr()
    }
    
    func ref(fullName : String, phone : String, email : String) -> Promise<Void> {
        return self.baseAPI(.post, path: "user-refs", body: ["type" : "SALE", "fullName" : fullName, "phone" : phone, "email" : email]).asVoid()
    }
    
    func getRefs(id: String?) -> Promise<Array<Ref>> {
        var path = "user-refs?$fields=*,referrer.*&$limit=10"
        if !Utils.isEmpty(id) {
            path.append("&$to=_id:\(id!)")
        }
        return self.baseAPI(.get, path: path, body: nil).asArr()
    }
    
    func logout() -> Promise<Void> {
        return self.baseAPI(.put, path: "auth/logout", body: nil).asVoid()
    }
    
    func addOrder(order : OrderRequest) -> Promise<Void> {
        return self.baseAPI(.post, path: "/customers", body: APIData(order.toJSON())).asVoid()
    }
    
    func checkVersion(version: String) -> Promise<Version> {
        return self.baseAPI(.post, path: "/auth/version", body: APIData([
            "version": version,
            "app": "IOS_SALE"
        ])).asObject()
    }
    
    func updateBankAccount(bankAccount: UserBankAccount, nationalCode: String) -> Promise<Void>{
        return self.baseAPI(.put, path: "/users/me/bank_account", body: APIData([
            "bank": bankAccount.bank,
            "accountNo": bankAccount.accountNo,
            "holderName": bankAccount.holderName,
            "branch": bankAccount.branch,
            "nationalCode": nationalCode
        ])).asVoid()
    }
    
    func newCashoutReq(amount: Int) -> Promise<Void>{
        return self.baseAPI(.post, path: "/cash-out-reqs", body: APIData([
            "amount": amount
        ])).asVoid()
    }
    
    func register(name: String, phone: String, password: String) -> Promise<Void> {
        return self.baseAPI(.post, path: "/auth/reg", body: APIData([
            "name": name,
            "phone": phone,
            "birth_day": 0,
            "password": password
            ])).asVoid()
    }
    
    func forgotPassword(info: String) -> Promise<Void> {
        return self.baseAPI(.post, path: "/auth/forgot-password", body: APIData([
            "info": info
            ])).asVoid()
    }
    
    func searchCustomerPhone(phone: String) -> Promise<Customer?> {
        return self.baseAPI(.get, path: "/customers?$fields=*&phone=\(phone)", body: nil).asArr().then({Promise.resolve($0.first)})
    }
    
    func cancelOrder(orderId: String) -> Promise<Void> {
        return self.baseAPI(.put, path: "/orders/\(orderId)/status/cancelled", body: nil).asVoid()
    }
}
