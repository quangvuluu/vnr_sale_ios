//
//  User.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/8/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class UserBankAccount: Mappable {
    init() {}
    required init?(map: Map) {
        if !NSObject.validField(UserBankAccount.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        bank <- map["bank"]
        accountNo <- map["accountNo"]
        holderName <- map["holderName"]
        branch <- map["branch"]
    }
    
    var bank: String = ""
    var accountNo: String = ""
    var holderName: String = ""
    var branch: String = ""
}

class User: Mappable {
    var _id : String = ""
    var phone : String = ""
    var fullName : String = ""
    var area : String = ""
    var nationalCode: String = ""
    var bankAccount: UserBankAccount?
    var roles: Array<String> = []
    
    init() {}
    required init?(map: Map) {
        if !NSObject.validField(User.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        _id <- map["map"]
        phone <- map["phone"]
        fullName <- map["fullName"]
        area <- map["area"]
        nationalCode <- map["nationalCode"]
        bankAccount <- map["bankAccount"]
        roles <- map["roles"]
    }
}
