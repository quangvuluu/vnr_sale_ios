//
//  OrderRequest.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/11/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderRequestOrder: Mappable {
    var amount : Array<AmountRequest> = []
    var note : String = ""
    
    init() {}
    
    required init?(map: Map) {
        if !NSObject.validField(OrderRequestOrder.self, map: map) {
            return nil
        }
    }
    
     func mapping(map: Map) {
        amount <- map["amount"]
        note <- map["note"]
    }
}

class OrderRequest: Mappable {
    var order : OrderRequestOrder = OrderRequestOrder()
    var receiver = Receiver()
    
    init() {}
    
    required init?(map: Map) {
        if !NSObject.validField(OrderRequest.self, map: map) {
            return nil
        }
    }
    
     func mapping(map: Map) {
        order <- map["order"]
        receiver <- map["customer"]
    }
}

class Receiver: Mappable {
    init() {}
    
    required init?(map: Map) {
        if !NSObject.validField(Receiver.self, map: map) {
            return nil
        }
    }
    
     func mapping(map: Map) {
        address <- map["address"]
        lat <- map["lat"]
        long <- map["long"]
        name <- map["fullName"]
        phone <- map["phone"]
        consumtion_rate <- map["consumtion_rate"]
        area <- map["area"]
        note <- map["note"]
    }
    
    var address : String = ""
    var lat : Double = 0.0
    var long : Double = 0.0
    var name : String = ""
    var phone : String = ""
    var consumtion_rate : Int = 0
    var area : String = ""
    var note : String = ""
}

