//
//  Ref.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/9/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Ref: Mappable {
    required init?(map: Map) {
        if !NSObject.validField(Ref.self, map: map) {
            return nil
        }
    }
    
     func mapping(map: Map) {
        _id <- map["_id"]
        fullName <- map["fullName"]
        phone <- map["phone"]
        email <- map["email"]
        referrer <- map["referrer"]
    }
    
    var _id : String = ""
    var fullName : String = ""
    var phone : String = ""
    var email : String = ""
    var referrer : Referrer?
}
