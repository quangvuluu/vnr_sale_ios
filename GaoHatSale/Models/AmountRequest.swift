//
//  AmountRequest.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/13/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class AmountRequest: Mappable {
    init(){}
    
    required init?(map: Map) {
        if !NSObject.validField(Amount.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        product <- map["product"]
        amount <- map["amount"]
    }
    
    var product : String = ""
    var amount : Int = 0
    var price : Int = 0
    
    init(product : String, amount : Int, price : Int) {
        self.product = product
        self.amount = amount
        self.price = price
    }
}
