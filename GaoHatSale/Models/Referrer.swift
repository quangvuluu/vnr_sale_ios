//
//  Referrer.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/9/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Referrer: Mappable {
    required init?(map: Map) {
        if !NSObject.validField(Referrer.self, map: map) {
            return nil
        }
    }
    
     func mapping(map: Map) {
        _id <- map["_id"]
        fullName <- map["fullName"]
        phone <- map["phone"]
    }
    
    var _id: String = ""
    var fullName: String = ""
    var phone : String = ""
    
    
}
