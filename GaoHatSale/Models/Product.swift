//
//  Product.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/7/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Product: Mappable {
    var code : String = ""
    var name : String = ""
    var des: String = ""
    var unit : String = ""
    var price : Int = 0
    
    required init?(map: Map) {
        if !NSObject.validField(Product.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        name <- map["name"]
        des <- map["description"]
        unit <- map["unit"]
        price <- map["price"]
    }
}
