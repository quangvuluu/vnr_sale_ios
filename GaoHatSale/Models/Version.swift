//
//  Version.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/20/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Version: NSObject, Mappable {
    required init?(map: Map) {
        if !NSObject.validField(Ref.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        url <- map["url"]
    }
    
    var success : Bool = true
    var url : String = ""
}
