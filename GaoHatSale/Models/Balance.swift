//
//  Balance.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/8/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Balance: Mappable {
    var _id : String = ""
    var balance : Int = 0
    var report : [String : Int] = [String : Int]()
    
    required init?(map: Map) {
        if !NSObject.validField(Balance.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        balance <- map["balance"]
        report <- map["report"]
    }
}
