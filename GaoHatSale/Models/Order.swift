//
//  Order.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/8/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderDeliverer: Mappable {
    required init?(map: Map) {
        if !NSObject.validField(OrderDeliverer.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        storeDescription <- map["storeDescription"]
    }
    
    var _id : String = ""
    var storeDescription: String = ""
}

class Order: Mappable {
    required init?(map: Map) {
        if !NSObject.validField(Order.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        _id <- map["_id"]
        total_amount <- map["total_amount"]
        total_price <- map["total_price"]
        status <- map["status"]
        expired_at <- map["expired_at"]
        created_at <- map["created_at"]
        customer <- map["customer"]
        amount <- map["amount"]
        note <- map["note"]
        cancelling_reason <- map["cancelling_reason"]
        deliverer <- map["deliverer"]
    }
    
    var code : String = ""
    var _id : String = ""
    var total_amount : Int = 0
    var total_price : Int = 0
    var status : String = ""
    var expired_at : Date = Date()
    var created_at : Date = Date()
    var customer : Customer?
    var amount : Array<Amount> = []
    var note: String = ""
    var cancelling_reason: String?
    var deliverer: OrderDeliverer?
    
    var productDescription: String {
        return self.amount.map({"\(Glob.getProduct(code: $0.product)?.name ?? ""): \($0.amount)kg"}).joined(separator: "\n")
    }
    
    var statusDescription: String {
        switch self.status {
            case "CANCELLED":
                return "Đã huỷ"

            case "DELIVERING":
                return "Chờ giao"

            case "DELIVERED":
                return "Đã giao"

            case "TIMED_OUT":
                return "Quá hạn"

            case "ERROR":
                return "Gặp lỗi"

            case "DELETED":
                return "Đã xoá"
            default:
                return "..."
        }
    }
    
    var statusColor: UIColor {
        switch self.status {
            case "CANCELLED":
                return UIColor.orange

            case "DELIVERING":
                return UIColor.GGGreen

            case "DELIVERED":
                return UIColor.GGGreen

            case "TIMED_OUT":
                return UIColor.GGRed

            case "ERROR":
                return UIColor.GGRed

            case "DELETED":
                return UIColor.GGRed
            default:
                return UIColor.GGRed
        }
    }
}

enum OrderStatus : String {
    case DELIVERED = "Đã giao"
    case CANCELLED = "Đã huỷ"
}
