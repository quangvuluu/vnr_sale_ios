//
//  Transaction.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/8/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Transaction: Mappable {
    var _id : String = ""
    var type : String = ""
    var create_at : Date = Date()
    var amount : Int = 0
    var meta_data : String = ""
    var content : String = ""
    
    required init?(map: Map) {
        if !NSObject.validField(Transaction.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        type <- map["type"]
        create_at <- (map["created_at"], NSObject.MilisecDateTransform())
        amount <- map["amount"]
        meta_data <- map["meta_data"]
        content <- map["content"]
    }
}
