//
//  Amount.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/8/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Amount: Mappable {
    
    init() {}
    
    required init?(map: Map) {
        if !NSObject.validField(Amount.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        product <- map["product"]
        amount <- map["amount"]
        price <- map["price"]
    }
    
    var product : String = ""
    var amount : Int = 0
    var price : Int = 0
}
