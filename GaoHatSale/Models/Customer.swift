//
//  Customer.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/8/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class Customer: Mappable {
    required init?(map: Map) {
        if !NSObject.validField(Customer.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        phone <- map["phone"]
        fullName <- map["fullName"]
        consumption_rate <- map["consumption_rate"]
        address <- map["address"]
        lat <- map["lat"]
        long <- map["long"]
        note <- map["note"]
    }
    
    var _id : String = ""
    var phone : String = ""
    var fullName : String = ""
    var consumption_rate : Double = 0.0
    var address : String = ""
    var lat : Double = 0.0
    var long : Double = 0.0
    var note : String = ""
}
