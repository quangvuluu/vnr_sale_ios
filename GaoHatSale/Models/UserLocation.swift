//
//  Location.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 4/8/19.
//  Copyright © 2019 kieuanh Nguyen. All rights reserved.
//

import UIKit
import ObjectMapper

class UserLocation: Mappable {
    var address: String = ""
    var lat : Double = 0.0
    var long : Double = 0.0
    
    required init?(map: Map) {
        if !NSObject.validField(UserLocation.self, map: map) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        address <- map["address"]
        lat <- map["lat"]
        long <- map["long"]
    }
}
