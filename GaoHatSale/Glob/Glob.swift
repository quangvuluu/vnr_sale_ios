//
//  Glob.swift
//  iqx
//
//  Created by Salm on 9/4/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class Glob {
    static var accessToken: String?
    static var scope : String?
    static var allProducts : Array<Product> = []
    static var user = User()
    
    static func reloadAllCaches() {
        accessToken = ""
        allProducts = []
        user = User()
    }
    
    static func getProduct(code: String) -> Product? {
        return self.allProducts.first(where: {$0.code == code})
    }
}
