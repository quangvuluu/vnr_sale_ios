//
//  DateFormat.swift
//  DoctorApp
//
//  Created by Salm on 5/11/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

class DateFormat: NSObject {
    
    static let iso8601Formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: Calendar.Identifier.iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSX"
        return formatter
    }()
    static let iso8601Transformer = Utils.createDateTransformer(iso8601Formatter)
    
    static let defaultDisplayFormat = "dd/MM/YY"
    static let defaultDisplayFormatter = Utils.createFormatter(DateFormat.defaultDisplayFormat)
    static let dateTimeFormat = "dd/MM/YY HH:mm"
    static let dateTimeFormatter = Utils.createFormatter(DateFormat.dateTimeFormat)
    
    static func formatMinuteTime(_ minute: Int, separator: String = ":") -> String {
        let h = minute / 60
        let m = minute % 60
        
        return "\(String(format: "%02d", h))\(separator)\(String(format: "%02d", m))"
    }
    
    static func weekDayName(_ weekDay: Int) -> String {
        switch weekDay {
        case 1:
            return "Chủ Nhật"
        case 2:
            return "Thứ 2"
        case 3:
            return "Thứ 3"
        case 4:
            return "Thứ 4"
        case 5:
            return "Thứ 5"
        case 6:
            return "Thứ 6"
        case 7:
            return "Thứ 7"
        default:
            return ""
        }
    }
}
