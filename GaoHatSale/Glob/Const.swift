//
//  Const.swift
//  iqx
//
//  Created by Salm on 8/27/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit
import ObjectMapper

struct Config {
    let API_SCHEMA: String = "https"
    let API_URL: String
    let API_PORT: Int
}

enum Environment: Int, CaseIterable {
    case Production = 0
    case Staging
    case Development
    case RemoteDev1
    case RemoteDev2
}

var ENV = Environment.Production

let ALL_CONFIGS = [
    Environment.Production: Config(API_URL: "vnr.concon.vn", API_PORT: 443),
    Environment.Staging: Config(API_URL: "stagvnr.concon.vn", API_PORT: 443),
//    Environment.Development: Config(API_URL: "localhost", API_PORT: 8655),
//    Environment.RemoteDev1: Config(API_URL: "192.168.0.31", API_PORT: 8655),
//    Environment.RemoteDev2: Config(API_URL: "10.255.211.81", API_PORT: 8655)
]

var CONFIG: Config {
    get {
        return ALL_CONFIGS[ENV]!
    }
}

class HC {
    static let DEFAULT_LOCATION: Location = (longitude: 106.69, latitude: 10.77)
    static var CURRENCY_FORMAT: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = "."
        return formatter
    }()
    
    static let AVAIL_CODES = (1...99).map({$0})
    static var COMMON_ITEM_DESCS: [String] = [];
    static let COMMON_PRICES = (2...40).map({$0 * 5000})
}

extension UIColor {
    convenience init(hexString: String, alpha:CGFloat? = 1.0) {
        // Convert hex string to an integer
        let hexint = Int(UIColor.intFromHexString(hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        
        // Create color object, specifying alpha as well
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    class func intFromHexString(_ hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    static let FBDefault = UIColor(hexString: "3B5998")
    static let BlueSoft = UIColor(hexString: "4670c9")
    static let FBLight = UIColor(hexString: "8B9DC3")
    static let FBWeak = UIColor(hexString: "DFE3EE")
    static let FBWhite = UIColor(hexString: "F7F7F7")
    static let GGRed = UIColor(hexString: "DB4437")
    static let GGGreen = UIColor(hexString: "0F9D58")
    static let GRAD_1 = UIColor(red: 139/255, green: 28/255, blue: 76/255, alpha: 1)
    static let GRAD_2 = UIColor(red: 183/255, green: 68/255, blue: 83/255, alpha: 1)
}

enum Gender: Int {
    case male = 0
    case female = 1
    
    static let Transformer = TransformOf<Gender, Int>(fromJSON: {$0 == nil ? nil : Gender(rawValue: $0!)}, toJSON: {$0?.rawValue})
}

enum Role: String {
    case user = "USER"
    case admin = "ADMIN"
    
    static let Transformer = TransformOf<Role, String>(fromJSON: {$0 == nil ? nil : Role(rawValue: $0!)}, toJSON: {$0?.rawValue})
}
