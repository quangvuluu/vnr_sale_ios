//
//  Utils.swift
//  DoctorApp
//
//  Created by Salm on 4/28/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit

class Utils: NSObject {
    class func isEmpty(_ o: Any?) -> Bool {
        if o == nil {
            return true
        }
        
        if let str = o as? String {
            return str.isEmpty
        }
        
        return false
    }
    
    class func val<T>(_ vals: T?...) -> T {
        for val in vals {
            if !Utils.isEmpty(val) {
                return val!
            }
        }
        
        assert(false)
        return vals.first!!
    }
    
    class func isEmptyOrZero(_ num: NSNumber?) -> Bool {
        return num == nil || num!.int32Value == 0
    }
    
    class func randomString(_ len : Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0..<len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString as String
    }
    
    class func random(_ min: Double = 0, _ max: Double) -> Double {
        return min + Double(Float(arc4random()) / Float(UINT32_MAX)) * (max - min)
    }
    
    class func isValidPhoneNumber(_ testStr: String?) -> Bool {
        if let testStr = testStr {
            let phoneRegEx = "\\d{9,11}"
            
            let phoneTest = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
            return phoneTest.evaluate(with: testStr)
        }
        
        return false
    }
    
    class func isValidEmail(_ testStr: String?) -> Bool {
        if let testStr = testStr {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
        
        return false
    }

    
    //MARK: Color
    class func UIColorFromRGB(red: Int, green:Int, blue:Int) -> UIColor {
        return UIColor(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    

    //
    class func invokeLater(_ block: @escaping ()->()) {
        DispatchQueue.main.async(execute: block)
    }
    
    class func later(_ block: @escaping ()->()) {
        DispatchQueue.main.async(execute: block)
    }
    
    class var versionStr: String {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        let build = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as! String
        let versionStr = "\(version).\(build)"
        return versionStr
    }
}


//MARK: Alert Helper
extension Utils {
    class func showOKAlertPanel(_ controller: UIViewController, title: String, msg: String, callback: ((UIAlertAction)->())? = nil) {
        let alert = UIAlertController(title:title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: callback))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    class func showYesNoAlertPanel(_ controller: UIViewController, title: String, msg: String, yes: String = "Yes", no: String = "No", callback: ((Bool)->())? = nil) {
        let alert = UIAlertController(title:title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: yes, style: .cancel, handler: { _ in
            callback?(true)
        }))
        
        alert.addAction(UIAlertAction(title: no, style: .default, handler: { _ in
            callback?(false)
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
    class func showOKInputAlertPanel(_ controller: UIViewController, title: String, msg: String, placeHolder: String, callback: ((String)->())? = nil) {
        let alert = UIAlertController(title:title, message: msg, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { tf in
            tf.placeholder = placeHolder
            tf.clearButtonMode = .whileEditing
        })
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            let text = alert.textFields?.first?.text ?? ""
            callback?(text)
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
    class func showYesNoInputAlertPanel(_ controller: UIViewController, title: String, msg: String, placeHolder: String, yes: String = "Yes", no: String = "No", callback: ((Bool, String)->())? = nil) {
        let alert = UIAlertController(title:title, message: msg, preferredStyle: .alert)
        alert.addTextField(configurationHandler: { tf in
            tf.placeholder = placeHolder
            tf.clearButtonMode = .whileEditing
        })
        
        alert.addAction(UIAlertAction(title: yes, style: .cancel, handler: { _ in
            let text = alert.textFields?.first?.text ?? ""
            callback?(true, text)
        }))
        
        alert.addAction(UIAlertAction(title: no, style: .default, handler: { _ in
            let text = alert.textFields?.first?.text ?? ""
            callback?(false, text)
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
}

//MARK: Date time helper
extension Utils {
    class func getNowTimestamp() -> Double {
        let currentDateTime = Date()
        
        return currentDateTime.timeIntervalSince1970
    }
    
    
    static func createFormatter(_ formatString: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = formatString
        return formatter
    }
    
    class func age(_ date: Date) -> Int {
        let now = Date()
        let calendar = Calendar.current
        
        return (calendar as NSCalendar).components(.year,
                                   from: date,
                                   to: now,
                                   options: []).year!
    }
}

extension String {
    var emptyCheck: String? {
        if Utils.isEmpty(self as AnyObject?) {
            return nil
        }
        
        return self
    }
    
//    func rangeFromNSRange(_ nsRange : NSRange) -> Range<String.Index>? {
////        let from16 = utf16.startIndex.advancedBy(nsRange.location, limit: utf16.endIndex)
//        let from16 = utf16.index(<#T##i: String.UTF16View.Index##String.UTF16View.Index#>, offsetBy: <#T##String.UTF16View.IndexDistance#>, limitedBy: <#T##String.UTF16View.Index#>)
//        let to16 = from16.advancedBy(nsRange.length, limit: utf16.endIndex)
//        if let from = String.Index(from16, within: self),
//            let to = String.Index(to16, within: self) {
//            return from ..< to
//        }
//        return nil
//    }
    
    func extractNameComps() -> (String, String, String) {
        var lastName = ""
        var midleName = ""
        var firstName = ""
        
        var comps = self.split(separator: " ")
        if comps.count == 0 {
            return (lastName, midleName, firstName)
        }
        
        if comps.count == 1 {
            firstName = String(comps[0])
            return (lastName, midleName, firstName)
        }
        
        lastName = String(comps.removeFirst())
        firstName = String(comps.removeLast())
        midleName = comps.map(String.init).joined(separator: " ")
        return (lastName, midleName, firstName)
    }
    
    var trimmedString: String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}

extension DateFormatter {
    func strFromDate(_ date: Date?) -> String? {
        if (!Utils.isEmpty(date as AnyObject?)) {
            return self.string(from: date!)
        }
        
        return nil
    }
}

extension Collection {
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Iterator.Element? {
        let contains = self.startIndex <= index && index < self.endIndex
        return contains ? self[index] : nil
    }
    
    func group<K: Hashable>(by key: (Iterator.Element) -> K) -> [K:[Iterator.Element]] {
        let allKeys: Set<K> = Set<K>(self.map(key))
        var dict: [K:[Iterator.Element]] = [:]
        for k in allKeys {
            dict[k] = self.filter({key($0) == k})
        }
        
        return dict
    }
    
    func sorted<K: Comparable>(byKey key: (Iterator.Element) -> K) -> [Iterator.Element] {
        return self.map({($0, key($0))}).sorted(by: {$0.1 < $1.1}).map({$0.0})
    }
}

extension UIViewController {
    
    func setNavigationTitle(_ title: String) {
        self.navigationController?.navigationBar.topItem?.title = title
    }
}

extension UINavigationController {
    func pushUniqueController<T: UIViewController>(_ type: T.Type, controller: T, animated: Bool = true) {
        if let index = self.viewControllers.index(where: {$0.isKind(of: type)}) {
            var newControllers = self.viewControllers
            newControllers.remove(at: index)
            newControllers.append(controller)
            
            self.setViewControllers(newControllers, animated: animated)
        }
        else {
            self.pushViewController(controller, animated: animated)
        }
    }
}

extension UIView {
    func dismissKeyboardOnTap(_ target: UIView? = nil, cancelTapInViews: Bool? = nil) {
        let _target = target ?? self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: _target, action: #selector(UIView._doDismissKeyboard))
        
        if let ccTap = cancelTapInViews {
            tap.cancelsTouchesInView = ccTap
        }
        
        self.addGestureRecognizer(tap)
    }
    
    @objc func _doDismissKeyboard() {
        self.endEditing(true)
    }
    
    static func gradientLayer(_ size: CGSize, startColor: UIColor, endColor: UIColor, startLocation: CGPoint, endLocation: CGPoint) -> UIView {
        let view: UIView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.startPoint = startLocation
        gradient.endPoint = endLocation
        view.layer.insertSublayer(gradient, at: 0)
        
        return view
    }
    
    func dropShadow(x: CGFloat, y: CGFloat, radius: CGFloat = 2) {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: x, height: y)
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
    }
}

extension CGPoint {
    var length: CGFloat {
        return sqrt(x * x + y * y)
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Int {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
    
    var thousandize: String {
        return (self / 1000).formattedWithSeparator
    }
}

typealias Location = (latitude: Double, longitude: Double)

extension Utils {
    class func dist(_ p1: Location, _ p2: Location) -> Double {
        let (lat1, lon1, lat2, lon2) = (p1.latitude, p1.longitude, p2.latitude, p2.longitude)
        let R: Double = 6371; // Radius of the earth in km
        let dLat = deg2rad(lat2-lat1);  // deg2rad below
        let dLon = deg2rad(lon2-lon1);
        let a = sin(dLat/2) * sin(dLat/2) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * sin(dLon/2) * sin(dLon/2)
        let c = 2 * atan2(sqrt(a), sqrt(1-a));
        let d = R * c; // Distance in km
        return d;
    }
    
    class func driveDist(_ p1: Location, _ p2: Location) -> Double {
        let lat = p1.latitude + p2.latitude
        let long = p1.longitude + p2.longitude
        let dist = self.dist(p1, p2)
        return dist * (1.5 + 0.025 * ((lat + long).truncatingRemainder(dividingBy: 10.0)))
    }
    
    private class  func deg2rad(_ deg: Double) -> Double {
        return deg * (Double.pi / 180)
    }
}

extension UITableView {
    func showEmptyMessage(message:String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.bounds.size.width, height: self.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
}
