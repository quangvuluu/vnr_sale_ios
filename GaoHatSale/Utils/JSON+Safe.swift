//
//  JSON+Safe.swift
//  DoctorApp
//
//  Created by Salm on 4/30/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit
import SwiftyJSON

extension JSON {
    var safeString: String {
        return self.safeString()
    }
    
    func safeString(_ defValue: String = "") -> String {
        if let str = self.string {
            return str
        }
        
        return ""
    }
    
    var safeInt: Int {
        return self.safeInt()
    }
    
    func safeInt(_ defaultValue: Int = 0) -> Int {
        if let i = self.int {
            return i
        }
        
        return defaultValue
    }
    
    func safeArray<T>(_ converter: (JSON) -> T) -> [T] {
        if let arr = self.array {
            return arr.map(converter)
        }
        
        return []
    }
    
    var safeArrayObject: [[String: Any]] {
        return self.safeArray({ data in
            return data.dictionaryObject ?? [:]
        })
    }
    
    var safeDictObject: [String: [String: Any]] {
        if let dict = self.dictionary {
            var result: [String: [String: Any]] = [:]
            for (k, v) in dict {
                result[k] = v.dictionaryObject ?? [:]
            }
            return result;
        }
        
        return [:]
    }
    
    func query(_ keyPath: String) -> JSON {
        if keyPath.isEmpty {
            return self
        }
        
        let keys = keyPath.components(separatedBy: ".")
        var data = self
        for key in keys {
            data = data[key]
        }
        
        return data
    }
    
    var print: String {
        return self.rawString(String.Encoding.utf8)!
    }
}

extension JSON {
    var arrayOfDicts: [[String: Any]]? {
        if let arr = self.array {
            return arr.map({$0.dictionaryObject!})
        }
        
        return nil
    }
}
