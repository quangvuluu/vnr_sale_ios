//
//  UIViewController+Reachability.swift
//  GaoHatSale
//
//  Created by kieuanh Nguyen on 8/22/20.
//  Copyright © 2020 kieuanh Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIViewController {
    var appDelegate: AppDelegate {
        get {
            return UIApplication.shared.delegate as! AppDelegate
        }
    }
    
    var isReachable: Bool {
        get {
            return NetworkReachabilityManager()!.isReachable
        }
    }
}
