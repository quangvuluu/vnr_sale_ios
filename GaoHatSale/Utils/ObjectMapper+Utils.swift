//
//  ObjectMapper+Validation.swift
//  StoraDemo
//
//  Created by Salm on 6/11/17.
//  Copyright © 2017 Salm. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyJSON

extension NSObject {
    class func validField(_ clazz: AnyClass, map: Map, ignoredFields: Set<String> = [], required: [String] = []) -> Bool {
        let data = JSON(map.JSON)
        let properties = NSObject.propertyNames(clazz)
        for property in properties {
            if ignoredFields.contains(property) {
                continue
            }
            
            if !data.query(property).exists() {
                return false
            }
        }
        
        for reqField in required {
            if !data.query(reqField).exists() {
                return false
            }
        }
        
        return true
    }
    
    class func checkMap(_ map: Map, fields: String...) -> Bool {
        let data = JSON(map.JSON)
        for field in fields {
            if !data.query(field).exists() {
                return false
            }
        }
        
        return true
    }
    
    static func clone<T: Mappable>(_ obj: T) -> T {
        let mapper = Mapper<T>()
        let json = mapper.toJSON(obj)
        return mapper.map(JSON: json)!
    }
    
    static func createDateTransformer(_ formatter: DateFormatter) -> TransformOf<Date, String> {
        return TransformOf<Date, String>(fromJSON: { s in
            if Utils.isEmpty(s) {
                return nil
            }
            
            return formatter.date(from: s!)
        }, toJSON: { d in
            if Utils.isEmpty(d) {
                return nil
            }
            
            return formatter.string(from: d!)
        })
    }
    
    static func MilisecDateTransform() -> TransformOf<Date, Double> {
        return TransformOf<Date, Double>(fromJSON: { s in
            if let time = s {
                return Date(timeIntervalSince1970: TimeInterval(time / 1000.0))
            }
            
            return nil
        }, toJSON: { d in
            if let date = d {
                return date.timeIntervalSince1970 * 1000
            }
            
            return nil
        })
    }
    
    static func secondDateTransform() -> TransformOf<Date, Double> {
        return TransformOf<Date, Double>(fromJSON: { s in
            if let time = s {
                return Date(timeIntervalSince1970: TimeInterval(time))
            }
            
            return nil
        }, toJSON: { d in
            if let date = d {
                return date.timeIntervalSince1970
            }
            
            return nil
        })
    }
}
