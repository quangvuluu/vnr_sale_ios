//
//  UIViewController+Masonry.swift
//  StoraDemo
//
//  Created by Salm on 6/11/17.
//  Copyright © 2017 Salm. All rights reserved.
//

import UIKit

extension UIViewController {
    func showPopup(_ popup: UIViewController, heightRatio: CGFloat = 0.6, widthRatio: CGFloat = 0.9) -> (UIView, UIViewController) {
        popup.willMove(toParent: self)
        
        let overlay = UIView(frame: self.view.frame)
        overlay.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 0.8)
        self.view.addSubview(overlay)
        overlay.mas_makeConstraints { make in
            _ = make?.edges.equalTo()(self.view)
        }
        
        popup.view.layer.cornerRadius = 20
        popup.view.layer.masksToBounds = true
        popup.view.layer.borderWidth = 1
        //        popup.view.layer.borderColor = LCColor.LightCyan.CGColor
        overlay.addSubview(popup.view)
        self.addChild(popup)
        
        popup.view.mas_makeConstraints { make in
            _ = make?.centerX.equalTo()(overlay.mas_centerX)
            _ = make?.centerY.equalTo()(overlay.mas_centerY)
            _ = make?.width.equalTo()(overlay.mas_width)?.multipliedBy()(widthRatio)
            _ = make?.height.equalTo()(overlay.mas_height)?.multipliedBy()(heightRatio)
        }
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.view.endEditing(true)
        
        popup.didMove(toParent: self)
        
        return (overlay, popup)
    }
    
    func closePopup(_ popup: (UIView, UIViewController)) {
        popup.1.willMove(toParent: nil)
        popup.0.removeFromSuperview()
        popup.1.removeFromParent()
        popup.1.didMove(toParent: nil)
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
}
