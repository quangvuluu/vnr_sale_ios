//
//  UIViewController+NVActIndicator.swift
//  StoraDemo
//
//  Created by Salm on 6/11/17.
//  Copyright © 2017 Salm. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import PromiseKit

class ActivityData {
    
}

extension UIViewController {
    var indicator: NVActivityIndicatorView? {
        return self.view.subviews.first(where: {$0 is NVActivityIndicatorView}) as? NVActivityIndicatorView
    }
    
    func startNVIndicator(data: ActivityData = ActivityData()) {
        self.stopNVIndicator() // stop previous indicator if there is
        
//        let w = 100.0
//        let h = 100.0
//        let x = (Double(self.view.frame.width) - w) / 2
//        let y = (Double(self.view.frame.height) - h) / 2
//        let indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), color: UIColor.lightGray)
//        indicator.center = self.view.center
//        indicator.startAnimating()
//        self.view.addSubview(indicator)
        
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(data, .none)
    }
    
    func stopNVIndicator() {
//        if let indicator = self.indicator {
//            indicator.removeFromSuperview()
//            indicator.stopAnimating()
//        }
    }
}

extension Promise {
    func stopNVIndicator(_ controller: UIViewController) -> Promise<T> {
        return self.ensure {
            controller.stopNVIndicator()
        }
    }
}
