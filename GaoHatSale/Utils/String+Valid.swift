//
//  XX.swift
//  DLive
//
//  Created by Dev 1 on 7/7/17.
//  Copyright © 2017 Salm. All rights reserved.
//

import UIKit

class AddressServ {
    static let Districts = ["an giang","ba ria","vung tau","bac giang","bac kan","bac lieu","bac ninh","ben tre","binh dinh","binh duong","binh phuoc","binh thuan","ca mau","cao bang","dak lak","dak nong","dien bien","dong nai","dong thap","gia lai","ha giang","ha nam","ha tinh","hai duong","hau giang","hoa binh","hung yen","khanh hoa","kien giang","kon tum","lai chau","lam dong","lang son","lao cai","long an","nam dinh","nghe an","ninh binh","ninh thuan","phu tho","quang binh","quang nam","quang ngai","quang ninh","quang tri","soc trang","son la","tay ninh","thai binh","thai nguyen","thanh hoa","thua thien hue","tien giang","tra vinh","tuyen quang","vinh long","vinh phuc","yen bai","phu yen","can tho","da nang","hai phong","ha noi","ho chi minh","hn","hcm","hp","dn","brvt","tth"];
    static let Counties = ["a luoi","an bien","an duong","an khe","an lao","an lao","an minh","an nhon","an phu","anh son","ayun pa","an thi","ba be","ba che","ba dinh","ba don","ba to","ba tri","ba vi","ba ria","ba thuoc","bac ai","bac lieu","bach long vi","bach thong","bao lac","bao lam","bao lam","bao loc","bao thang","bao yen","bat xat","bau bang","bac binh","bac giang","bac ha","bac kan","bac me","bac ninh","bac quang","bac son","bac tan uyen","bac tra my","bac tu liem","bac yen","ben cat","ben cau","ben luc","ben tre","bien hoa","bim son","binh chanh","binh dai","binh gia","binh giang","binh lieu","binh long","binh luc","binh minh","binh son","binh tan","binh tan","binh thanh","binh thuy","binh xuyen","bo trach","bu dang","bu dop","bu gia map","buon don","buon ho","buon ma thuot","ca mau","cai lay","cai lay","cai be","cai nuoc","cai rang","cam lam","cam lo","cam ranh","can loc","cang long","cao bang","cao lanh","cao lanh","cao loc","cao phong","cat hai","cat tien","cam giang","cam khe","cam le","cam my","cam pha","cam thuy","cam xuyen","can duoc","can gio","can giuoc","cau giay","cau ke","cau ngang","chau doc","chau duc","chau phu","chau thanh","chau thanh","chau thanh","chau thanh","chau thanh","chau thanh","chau thanh","chau thanh","chau thanh","chau thanh","chau thanh a","chi lang","chi linh","chiem hoa","cho don","cho gao","cho lach","cho moi","cho moi","chon thanh","chu pah","chu prong","chu puh","chu se","chuong my","con cuong","co to","con dao","con co","co do","cu lao dung","cu chi","cu kuin","cu jut","cu m gar","cua lo","dau tieng","di linh","di an","dien khanh","dien chau","duy tien","duy xuyen","duyen hai","duyen hai","duong kinh","duong minh chau","da krong","da bac","da lat","da huoai","da teh","dai loc","dai tu","dak doa","dak po","dan phuong","dak glei","dak glong","dak ha","dak mil","dak r lap","dak song","dak to","dam doi","dam ha","dam rong","dat do","dien ban","dien bien","dien bien dong","dien bien phu","dinh lap","dinh hoa","dinh quan","doan hung","do luong","do son","dong anh","dong giang","dong ha","dong hai","dong hoa","dong hung","dong son","dong trieu","dong hoi","dong hy","dong phu","dong van","dong xoai","dong xuan","dong da","don duong","duc co","duc hoa","duc hue","duc linh","duc pho","duc tho","duc trong","ea h leo","ea kar","ea sup","gia binh","gia lam","gia loc","gia nghia","gia vien","gia rai","giang thanh","giao thuy","gio linh","giong rieng","giong trom","go cong","go cong dong","go cong tay","go dau","go quao","go vap","ha dong","ha giang","ha quang","ha tien","ha tinh","ha trung","ha hoa","ha lang","ha long","hai ba trung","hai an","hai chau","hai duong","hai ha","hai hau","hai lang","ham tan","ham thuan bac","ham thuan nam","ham yen","hau loc","hiep duc","hiep hoa","hoa lu","hoa an","hoa binh","hoa binh","hoa thanh","hoa vang","hoai an","hoai duc","hoai nhon","hoan kiem","hoang mai","hoang mai","hoang sa","hoang su phi","hoanh bo","hoang hoa","hoc mon","hon dat","hon quan","hoi an","hong bang","hong dan","hong linh","hong ngu","hong ngu","hue","hung ha","hung nguyen","hung yen","huong khe","huong son","huong thuy","huong tra","huong hoa","huu lung","ia grai","ia h drai","ia pa","kbang","ke sach","khanh son","khanh vinh","khoai chau","kien hai","kien luong","kien an","kien thuy","kien xuong","kien tuong","kim bang","kim boi","kim dong","kim son","kim thanh","kinh mon","kon plong","kon ray","kon tum","kong chro","krong ana","krong bong","krong buk","krong nang","krong no","krong pa","krong pak","ky anh","ky anh","ky son","ky son","la gi","lac duong","lac son","lac thuy","lai chau","lai vung","lang chanh","lang giang","lang son","lao cai","lak","lam binh","lam ha","lam thao","lap vo","lap thach","le chan","le thuy","lien chieu","long bien","long dien","long ho","long khanh","long my","long my","long phu","long thanh","long xuyen","loc binh","loc ha","loc ninh","luc nam","luc ngan","luc yen","luong son","luong tai","ly nhan","ly son","mai chau","mai son","mang thit","mang yang","m drak","meo vac","me linh","minh hoa","minh long","mo cay bac","mo cay nam","mong cai","mo duc","moc chau","moc hoa","mu cang chai","muong ang","muong cha","muong khuong","muong la","muong lat","muong lay","muong nhe","muong te","my duc","my hao","my loc","my tho","my tu","my xuyen","na hang","na ri","nam dan","nam dinh","nam dong","nam giang","nam sach","nam tra my","nam truc","nam tu liem","nam can","nam po","nam nhun","nga son","nga bay","nga nam","ngan son","nghi loc","nghi xuan","nghia dan","nghia hanh","nghia hung","nghia lo","ngoc hien","ngoc hoi","ngoc lac","ngo quyen","ngu hanh son","nguyen binh","nha trang","nha be","nho quan","nhon trach","nhu thanh","nhu xuan","ninh binh","ninh giang","ninh hai","ninh hoa","ninh kieu","ninh phuoc","ninh son","nong cong","nong son","nui thanh","o mon","pac nam","phan rang-thap cham","phan thiet","phong dien","phong dien","phong tho","pho yen","phu binh","phu giao","phu hoa","phu loc","phu luong","phu nhuan","phu ninh","phu quy","phu quoc","phu rieng","phu tan","phu tan","phu thien","phu tho","phu vang","phu xuyen","phu cat","phu cu","phu my","phu ninh","phu yen","phu ly","phuc tho","phuc yen","phuc hoa","phung hiep","phuoc long","phuoc long","phuoc son","pleiku","quan hoa","quan son","quan ba","quang binh","quang dien","quang ngai","quang ninh","quang trach","quang tri","quang yen","quang uyen","quang xuong","quan 1","quan 2","quan 3","quan 4","quan 5","quan 6","quan 7","quan 8","quan 9","quan 10","quan 11","quan 12","que phong","que son","que vo","quy nhon","quoc oai","quy chau","quy hop","quynh luu","quynh nhai","quynh phu","rach gia","sa dec","sa pa","sa thay","sam son","si ma cai","sin ho","soc son","soc trang","song cau","song cong","song hinh","song lo","song ma","sop cop","son dong","son duong","son ha","son hoa","son la","son tay","son tay","son tinh","son tra","tam binh","tam duong","tam dao","tam diep","tam duong","tam ky","tam nong","tam nong","tanh linh","tan an","tan bien","tan binh","tan chau","tan chau","tan hiep","tan hong","tan hung","tan ky","tan lac","tan phu","tan phu","tan phu dong","tan phuoc","tan son","tan thanh","tan thanh","tan tru","tan uyen","tan uyen","tan yen","tay giang","tay hoa","tay ho","tay ninh","tay son","tay tra","thach an","thach ha","thach thanh","thach that","thai binh","thai hoa","thai nguyen","thai thuy","than uyen","thanh ba","thanh binh","thanh chuong","thanh ha","thanh hoa","thanh khe","thanh liem","thanh mien","thanh oai","thanh son","thanh thuy","thanh tri","thanh xuan","thanh hoa","thanh phu","thanh tri","thap muoi","thang binh","thieu hoa","tho xuan","thoai son","thong nong","thong nhat","thot not","thoi binh","thoi lai","thu dau mot","thu duc","thu thua","thuan an","thuan bac","thuan chau","thuan nam","thuan thanh","thuy nguyen","thuong tin","thuong xuan","tien du","tien hai","tien lang","tien lu","tien phuoc","tien yen","tieu can","tinh gia","tinh bien","tra bong","tra cu","tra linh","tra on","tra vinh","tram tau","trang dinh","trang bang","trang bom","tran yen","tran de","tran van thoi","tri ton","trieu phong","trieu son","trung khanh","truc ninh","truong sa","tua chua","tuan giao","tu mo rong","tuy an","tuy duc","tuy hoa","tuy phong","tuy phuoc","tuyen hoa","tuyen quang","tu nghia","tu ky","tu son","tuong duong","u minh","u minh thuong","uong bi","ung hoa","van ninh","van ban","van chan","van giang","van lang","van lam","van quan","van yen","van canh","van don","van ho","vi thanh","vi thuy","vi xuyen","viet tri","viet yen","vinh","vinh bao","vinh chau","vinh cuu","vinh hung","vinh linh","vinh long","vinh loc","vinh loi","vinh thanh","vinh thanh","vinh thuan","vinh tuong","vinh yen","vo nhai","vu quang","vu thu","vu ban","vung liem","vung tau","xin man","xuan loc","xuan truong","xuyen moc","y yen","yen bai","yen binh","yen chau","yen dung","yen dinh","yen khanh","yen lac","yen lap","yen minh","yen mo","yen my","yen phong","yen son","yen thanh","yen the","yen thuy"];
    static let AddrWords = [["word":"xa","score":5],["word":"x.","score":3],["word":"phuong","score":7],["word":"p.","score":5],["word":"quan","score":7],["word":"q.","score":5],["word":"huyen","score":7],["word":"h.","score":5],["word":"thanh pho","score":10],["word":"tp.","score":7],["word":"thon","score":5],["word":"ap","score":2],["word":"duong","score":5]]
}

extension String {
    func replace(_ pattern: String, _ r: String) -> String {
        let regex = try! NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        return regex.stringByReplacingMatches(in: self, options: .withoutAnchoringBounds, range: NSMakeRange(0, self.count), withTemplate: r)
    }
    
    func containsRegEx(_ pattern: String) -> Bool {
        let regex = try! NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        return regex.firstMatch(in: self, options: .withTransparentBounds, range: NSMakeRange(0, self.count)) != nil
    }
    
    var standardlized: String {
        var str = self.lowercased()
        str = str.replace("à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ","a");
        str = str.replace("è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ","e");
        str = str.replace("ì|í|ị|ỉ|ĩ","i");
        str = str.replace("ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ","o");
        str = str.replace("ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ","u");
        str = str.replace("ỳ|ý|ỵ|ỷ|ỹ","y");
        str = str.replace("đ","d");
        str = str.replace("!|@|%|\\^|\\*|\\(|\\)|\\+|\\=|\\<|\\>|\\?|\\/|,|\\.|\\:|\\;|\\'| |\\\"|\\&|\\#|\\[|\\]|~|$|_"," ");
        str = str.replace("\\s+\\s"," ");
        str = str.replace("^\\s+|\\s+$","");
        return str;
    }
    
    func isAddressLike() -> Bool {
        let std = self.standardlized
        var score = 0
        let district = AddressServ.Districts.first(where: {std.contains($0)}) ?? ""
        let county = AddressServ.Counties.first(where: {std.contains($0)}) ?? ""
        
        score += district.count * 2
        score += county.count * 2
        
        let words = AddressServ.AddrWords.filter({std.contains($0["word"] as! String)})
        for word in words {
            score += word["score"] as! Int
        }
        
        return score >= 20
    }
    
    func isContainsPhoneNumber() -> Bool {
        return self.containsRegEx("\\d{9,}")
    }
}
