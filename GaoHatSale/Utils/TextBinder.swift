//
//  MMTextBinder.swift
//  MyMediSquare Doctor
//
//  Created by Vinova on 1/14/16.
//  Copyright © 2016 Vinova. All rights reserved.
//

import UIKit

enum MMTextBinderEvent {
    case shouldBeginEditing
    case didBeginEditing
    case shouldEndEditing
    case shouldChangeCharactersInRange
    case textShouldChange
    case shouldClear
    case shuldReturn
}

class TextBinder: NSObject {
    typealias TextFieldShouldBeginEditingFunction = (_ textField: UITextField) -> Bool
    typealias TextFieldDidBeginEditingFunction = (_ textField: UITextField) -> Void
    typealias TextFieldShouldEndEditingFunction = (_ textField: UITextField) -> Bool
    typealias TextFieldDidEndEditingFunction = (_ textField: UITextField) -> Void
    typealias TextFieldShouldChangeCharactersInRangeFunction = (_ textField: UITextField, _ range: NSRange, _ replaceString: String) -> Bool
    typealias TextFieldShouldChangeFunction = (_ textField: UITextField, _ text: String?) -> Bool
    typealias TextFieldShouldClearFunction = (_ textField: UITextField) -> Bool
    typealias TextFieldShouldReturnFunction = (_ textField: UITextField) -> Bool
    
    fileprivate class Binder {
        var shouldBeginEditing: TextFieldShouldBeginEditingFunction?
        var didBeginEditing: TextFieldDidBeginEditingFunction?
        var shouldEndEditing: TextFieldShouldEndEditingFunction?
        var didEndEditing: TextFieldDidEndEditingFunction?
        var shouldChangeCharacterInRange: TextFieldShouldChangeCharactersInRangeFunction?
        var textShouldChange: TextFieldShouldChangeFunction?
        var shouldClear: TextFieldShouldClearFunction?
        var shouldReturn: TextFieldShouldReturnFunction?
        
        var isEmpty: Bool {
            return (shouldBeginEditing == nil) &&
                (didBeginEditing == nil) &&
                (shouldEndEditing == nil) &&
                (didEndEditing == nil) &&
                (shouldChangeCharacterInRange == nil) &&
                (textShouldChange == nil) &&
                (shouldClear == nil) &&
                (shouldReturn == nil)
        }
    }
    
    fileprivate var binds = [UITextField: Binder]()
    
    private func getBinderForTextField(_ textField: UITextField) -> Binder {
        textField.delegate = self
        
        if let binder = binds[textField] {
            return binder
        }
        
        let binder = Binder()
        binds[textField] = binder
        return binder
    }
    
    func bindTextField(_ textField: UITextField, shouldBeginEditing: TextFieldShouldBeginEditingFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.shouldBeginEditing = shouldBeginEditing
    }
    
    func bindTextField(_ textField: UITextField, didBeginEditing: TextFieldDidBeginEditingFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.didBeginEditing = didBeginEditing
    }
    
    
    func bindTextField(_ textField: UITextField, shouldEndEditing: TextFieldShouldEndEditingFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.shouldEndEditing = shouldEndEditing
    }
    
    
    func bindTextField(_ textField: UITextField, didEndEditing: TextFieldDidEndEditingFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.didEndEditing = didEndEditing
    }
    
    
    func bindTextField(_ textField: UITextField, shouldChangeCharacterInRange: TextFieldShouldChangeCharactersInRangeFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.shouldChangeCharacterInRange = shouldChangeCharacterInRange
    }
    
    
    func bindTextField(_ textField: UITextField, textShouldChange: TextFieldShouldChangeFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.textShouldChange = textShouldChange
    }
    
    
    func bindTextField(_ textField: UITextField, shouldClear: TextFieldShouldClearFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.shouldClear = shouldClear
    }
    
    
    func bindTextField(_ textField: UITextField, shouldReturn: TextFieldShouldReturnFunction?) {
        let binder = self.getBinderForTextField(textField)
        binder.shouldReturn = shouldReturn
    }
    
    func unbind(_ textField: UITextField) {
        textField.delegate = nil
        binds.removeValue(forKey: textField)
    }
    
    func unbindResc(_ view: UIView) {
        if let tf = (view as? UITextField) {
            self.unbind(tf)
        }
        
        for subView in view.subviews {
            self.unbindResc(subView)
        }
    }
    
    func unbindTextFieldShouldBeginEditing(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.shouldBeginEditing = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    
    func unbindTextFieldDidBeginEditing(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.didBeginEditing = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    
    func unbindTextFieldShouldEndEditing(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.shouldEndEditing = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    
    func unbindTextFielDdidEndEditing(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.didEndEditing = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    
    func unbindTextFieldShouldChangeCharacterInRange(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.shouldChangeCharacterInRange = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    
    func unbindTextFieldTextShouldChange(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.textShouldChange = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    
    func unbindTextFieldShouldClear(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.shouldClear = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    
    func unbindTextFieldShouldReturn(_ textField: UITextField) {
        textField.delegate = nil
        if let binder = self.binds[textField] {
            binder.shouldReturn = nil
            if (binder.isEmpty) {
                self.binds.removeValue(forKey: textField)
            }
        }
    }
    
    deinit {
        for textField in binds {
            textField.0.delegate = nil
        }
    }
}

extension TextBinder: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let binder = self.binds[textField] {
            if let _func = binder.shouldBeginEditing {
                return _func(textField)
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let binder = self.binds[textField] {
            if let _func = binder.didBeginEditing {
                _func(textField)
            }
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let binder = self.binds[textField] {
            if let _func = binder.shouldEndEditing {
                return _func(textField)
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let binder = self.binds[textField] {
            if let _func = binder.didEndEditing {
                _func(textField)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var ret = true
        if let binder = self.binds[textField] {
            if let _func = binder.shouldChangeCharacterInRange {
                ret = _func(textField, range, string)
            }
            
            if let _func = binder.textShouldChange {
                let nsString = textField.text as NSString?
                let str = nsString?.replacingCharacters(in: range, with: string)
                ret = _func(textField, str)
            }
        }
        
        return ret
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let binder = self.binds[textField] {
            if let _func = binder.shouldClear {
                return _func(textField)
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let binder = self.binds[textField] {
            if let _func = binder.shouldReturn {
                return _func(textField)
            }
        }
        
        return true
    }
}

extension TextBinder {
    @nonobjc static let DisableKeyboard: TextFieldShouldBeginEditingFunction = {_ in false}
    @nonobjc static let DismissKeyboardOnReturn: TextFieldShouldReturnFunction = {tf in tf.resignFirstResponder() }
}
