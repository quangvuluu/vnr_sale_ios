//
//  UIView+Masonry.swift
//  StoraDemo
//
//  Created by Salm on 6/11/17.
//  Copyright © 2017 Salm. All rights reserved.
//

import UIKit
import Masonry

extension UIView {
    func addUnderline(_ weight: CGFloat = 1, color: UIColor, leftInset: CGFloat = -2, rightInset: CGFloat = 2, spacing: CGFloat = 1) {
        let line = UIView()
        line.backgroundColor = color;
        self.superview!.addSubview(line)
        line.mas_makeConstraints { make in
            _ = make?.height.equalTo()(weight)
            _ = make?.left.equalTo()(self.mas_left)?.valueOffset()?(leftInset as NSValue?)
            _ = make?.right.equalTo()(self.mas_right)?.valueOffset()?(-rightInset as NSValue?)
            _ = make?.top.equalTo()(self.mas_bottom)?.valueOffset()?(spacing as NSValue?)
        }
    }
    
    func fit(_ subView: UIView) {
        assert(self.subviews.contains(subView))
        
        subView.mas_makeConstraints({ mk in
            _ = mk?.edges.equalTo()(self)
        })
    }
    
    func addSeparator() {
        let separatorHeight: CGFloat = 4
        let frame = CGRect(x: 0, y: bounds.height - separatorHeight, width: bounds.width, height: separatorHeight)
        let separator = UIView(frame: frame)
        separator.backgroundColor = .gray
        separator.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
        
        addSubview(separator)
    }
}
