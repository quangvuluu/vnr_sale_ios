//
//  ContainerView.swift
//  PatientApp
//
//  Created by Salm on 5/23/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import UIKit

class ContainerView: UIView {
    @IBOutlet var ownerController: UIViewController!
    private(set) var embededController: UIViewController?
    
    func unembedViewController() -> UIViewController? {
        assert(self.subviews.count <= 1, "Container can only have at most one subview!")
        
        if let vc = self.embededController {
            let subView = self.subviews.first!
            assert(subView == vc.view, "Invalid subview")
            
            vc.willMove(toParent: nil)
            subView.removeFromSuperview()
            vc.didMove(toParent: nil)
            
            self.embededController = nil
            
            return vc
        }
        
        return nil
    }
    
    func embedViewController(_ viewController: UIViewController?) -> UIViewController? {
        assert(self.subviews.count <= 1, "Container can only have at most one subview!")
        if (viewController == self.embededController) {
            return nil
        }
        
        let oldVC = self.unembedViewController()
        
        // Add view to placeholder view
        if let vc = viewController {
            self.embededController = vc
            let view = vc.view
            
            vc.willMove(toParent: self.ownerController)
            self.addSubview(view!)
            
            self.translatesAutoresizingMaskIntoConstraints = false
            view?.translatesAutoresizingMaskIntoConstraints = false
            
            self.fit(view!)
            
            self.setNeedsLayout()
            self.layoutIfNeeded()
            
            self.ownerController.addChild(vc)
            vc.didMove(toParent: self.ownerController)
        }
        
        return oldVC
    }
}
