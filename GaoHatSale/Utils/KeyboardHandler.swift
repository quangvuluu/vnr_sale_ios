//
//  MMKeyboardHandler.swift
//  MyMediSquare Doctor
//
//  Created by Vinova on 2/17/16.
//  Copyright © 2016 Vinova. All rights reserved.
//

import UIKit

class KeyboardHandler: NSObject {
    weak var view: UIView!
    weak var bottomConstraint: NSLayoutConstraint!
    private var oldConst: CGFloat = 0
    private var isOldConsSet: Bool = false;
    private var keyboardHeight: CGFloat = 0
    
    var keyboardWasShown: ((Bool) -> Void)?
    var keyboardWasHidden: ((Bool) -> Void)?
    
    required init(view: UIView, bottomConstr: NSLayoutConstraint) {
        self.view = view
        self.bottomConstraint = bottomConstr
    }
    
    func attachListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHandler.keyboardWasShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHandler.keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHandler.keyboardWillChangeFrameNotification(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardHandler.keyboardDidChangeFrameNotification(_:)), name: UIResponder.keyboardDidChangeFrameNotification, object: nil)
    }
    
    func removeListener() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWasShown(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let duration = info.object(forKey: UIResponder.keyboardAnimationDurationUserInfoKey) as! NSNumber
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        keyboardHeight = max(keyboardSize!.height, keyboardHeight)
        
        let frame = view.convert(view.bounds, to: nil)
        let screenHeight = view.window!.frame.size.height
        let bottomGap = screenHeight - frame.origin.y - frame.size.height
        
        //---
        if (!isOldConsSet) {
            oldConst = bottomConstraint.constant
            isOldConsSet = true
            bottomConstraint.constant = keyboardHeight - bottomGap + oldConst + 10
            UIView.animate(withDuration: duration.doubleValue, animations: {
                self.view.layoutIfNeeded()
                }, completion: { finished in
                    self.keyboardWasShown?(finished)
            })
        }
    }
    
    @objc func keyboardWillBeHidden(_ notification: Notification)
    {
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let duration = info.object(forKey: UIResponder.keyboardAnimationDurationUserInfoKey) as! NSNumber
        //---
        bottomConstraint.constant = oldConst
        isOldConsSet = false
        UIView.animate(withDuration: duration.doubleValue, animations: {
            self.view.layoutIfNeeded()
            }, completion: { finished in
                self.keyboardWasHidden?(finished)
        })
    }
    
    @objc func keyboardWillChangeFrameNotification(_ notification: Notification) {
        debugPrint("frame will change....")
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.keyboardHeight = keyboardSize!.height
    }
    
    @objc func keyboardDidChangeFrameNotification(_ notif: Notification) {
        debugPrint("frame did change.....")
    }
}
