//
//  Promise+Unnamed.swift
//  iqx
//
//  Created by Salm on 12/11/16.
//  Copyright © 2016 Salm. All rights reserved.
//

import PromiseKit

extension Promise {
    
    public func mapPromise<U>(_ body: @escaping (T) -> PromiseKit.Promise<U>) -> PromiseKit.Promise<U> {
        return self.then { (val) -> Promise<U> in
            return body(val)
        }
    }
    
//    public func `catch`(_ body: @escaping (Error) -> Swift.Void) -> PromiseKit.Promise<T> {
//        return self.catch(body)
//    }
//    
//    public func `error`(_ body: @escaping (Error) -> Swift.Void) -> PromiseKit.Promise<T> {
//        return self.catch(body)
//    }
}
