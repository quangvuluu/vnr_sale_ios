//
//  SocketIO+Promise.swift
//  DLive
//
//  Created by Salm on 6/27/17.
//  Copyright © 2017 Salm. All rights reserved.
//

//import UIKit
//import PromiseKit
//import ObjectMapper
//import SwiftyJSON
//
//struct SocketIOError: Error {
//    let socket: SocketIOClient
//}
//
//typealias ObjectHandler<T> = (T) -> Void
//typealias ArrayHandler<T> = ([T]) -> Void
//
//extension SocketIOClient {
//    func connect(timeout: Int) -> Promise<Void> {
//        return Promise<Void>(resolvers: { resolve, reject in
//            self.connect(timeoutAfter: timeout, withHandler: {
//                debugPrint("Here....");
//                let err = SocketIOError(socket: self)
//                reject(err)
//            })
//
//            self.once("connect", callback: { args in
//                if (self.status == .connected) {
//                    debugPrint(args)
//                    resolve()
//                    return
//                }
//
//                let err = SocketIOError(socket: self)
//                reject(err)
//            })
//        })
//    }
//
//    var clientID: String? {
//        return self.engine?.sid
//    }
//
//    func on<T: Mappable>(_ event: String, handler: @escaping ObjectHandler<T>) {
//        let mapper = Mapper<T>()
//        self.onJSON(event, handler: { json in
//            let dict = json.dictionaryObject
//            if dict != nil, let obj = mapper.map(JSON: dict!)  {
//                handler(obj)
//            }
//        })
//    }
//
//    func on<T: Mappable>(_ event: String, handler: @escaping ArrayHandler<T>) {
//        let mapper = Mapper<T>()
//        self.onJSON(event, handler: { json in
//            let arr = json.safeArray { $0.dictionaryObject != nil ? mapper.map(JSON: $0.dictionaryObject!) : nil }
//            let filtered = arr.filter({$0 != nil}).map({$0!})
//            handler(filtered)
//        })
//        self.on(event, callback: { args, _ in
//        })
//    }
//
//    func onJSON(_ event: String, handler: @escaping ObjectHandler<JSON>) {
//        self.on(event, callback: { args, _ in
//            if let data = args.first {
//                let json = JSON(data)
//                handler(json)
//            }
//        })
//    }
//}

